import threading
import socket
import sys, random, string
import logging, configparser

# get information from config file
filename = 'configServer.ini'
config = configparser.ConfigParser()
config.read(filename)


# Config of the log file
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')

file_handler = logging.FileHandler(config['INFO']['logFileName'])
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


# host = "127.0.0.1" # localhost just for testing
host = ''
port = int(config['DATA']['Port'])

# Constant storing size of buffer
BUFF_SIZE = 1024

# CREATE SERVER SOCKET
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Display message of server starting
print("Starting server...")
logger.info("Starting server...")

# BIND the server
server.bind((host, port))
server.listen()
# Display message of listening port
print(f"Server listening on port {port}")
logger.info(f"Server listening on port {port}")

clients   = []
usernames = []

paused_clients = []

private_conversations = {}

FILE_TRANSFER = {}

def check_if_tokens_available(token):

    for user in private_conversations:
        for conv in private_conversations[user]:
            if list(conv.values())[0] == token:
                return False
    
    return True

def generate_unique_token():
    length = 6

    while True:
        token = ''.join(random.choices(string.ascii_uppercase, k=length))
        if check_if_tokens_available(token):
            break

    return token

# Function to send a response to all users connected
def broadcast(response):
    for client in clients:
        client.send(response)

# Function to send a message to all users connected in the public conversation room
def broadcast_msg(message, username):
    response = f'40==0;{username};"{message}"'.encode('utf-8')
    for client in clients:
        client.send(response)

def check_if_client_exists(name):
    for username in usernames:
        if username == name:
            return True
    
    return False

def send_list_clients(client):
    str_list = ';'.join(usernames)
    response = "0==LIST_" + str_list
    broadcast(response.encode('utf-8'))

def rename_client(client, req):
    if len(req) < 9:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        old_name_index = clients.index(client)
        old_username = usernames[old_name_index]
        new_username = req.split()[1]
    
        if not check_if_client_exists(new_username):
            usernames[old_name_index] = new_username
            broadcast(f"50=={old_username};{new_username}".encode('utf-8'))
            # client.send('Username updated succefully.'.encode('utf-8'))
            client.send(f'0==RENAME_OK;{old_username};{new_username}'.encode('utf-8'))
        else:
            client.send('10==INVALID_USERNAME'.encode('utf-8'))

def send_private_conversation_demand(client, req):
    if len(req) < 5:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        
        username_receiver = req.split()[1]
        if username_receiver == username_sender:
            client.send('27==CANNOT_START_PRIVATE_CONVERSATION_WITH_YOURSELF'.encode('utf-8'))
        elif not check_if_client_exists(username_receiver):
            client.send("11==USERNAME_DO_NOT_EXIST".encode('utf-8'))
        else:
            username_receiver_index = usernames.index(username_receiver)
            client_receiver = clients[username_receiver_index]
            
            new_room = {username_receiver: None}
            if username_sender in private_conversations:
                if private_conversations[username_sender].count(new_room) == 0:
                    private_conversations[username_sender].append(new_room)
            else:
                private_conversations[username_sender] = [{username_receiver: None}]
            
            client_receiver.send(f'41==DM_REQUEST_RECEIVED;{username_sender}'.encode('utf-8'))

# def check_if_DM_sent(username):
#     return username in private_conversations

def check_if_DM_sent(username_src, username_dest):
    try:
        for dm in private_conversations.get(username_src):
            if username_dest in dm:
                return True
        return False
    except:
        return False

def update_room_token(username_receiver, username_sender, token):
    for pv in private_conversations.get(username_receiver):
        if username_sender in pv:
            pv[username_sender] = token
            break

def delete_private_conversation(username_receiver, username_sender):
    # print(private_conversations)
    for pv in private_conversations.get(username_receiver):
        if username_sender in pv:
            private_conversations[username_receiver].remove(pv)
            break

def check_if_room_valid(username_sender, token):
    for key, value in private_conversations.items():
        for d in value:
            if list(d.values())[0] == token:
                key2 = list(d.keys())[0]
                if key == username_sender or key2 == username_sender:
                    return key, key2
    
    return None, None

def private_conversation_accepted(client, req):
    if len(req) < 12:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        
        username_receiver = req.split()[1]
        
        if not check_if_client_exists(username_receiver):
            client.send("11==USERNAME_DO_NOT_EXIST".encode('utf-8'))
        elif username_receiver == username_sender:
            client.send('27==CANNOT_ACCEPT_PRIVATE_CONVERSATION_WITH_YOURSELF'.encode('utf-8'))
        elif not check_if_DM_sent(username_receiver, username_sender):
            client.send(f"30=={username_receiver}_DID_NOT_SEND_REQUEST".encode('utf-8'))
        else:
            
            username_receiver_index = usernames.index(username_receiver)
            client_receiver = clients[username_receiver_index]
            
            token = generate_unique_token()
            update_room_token(username_receiver, username_sender, token)
            
            client.send(f'43=={token};{username_receiver}'.encode('utf-8'))
            client_receiver.send(f'43=={token};{username_sender}'.encode('utf-8'))

def private_conversation_refused(client, req):
    if len(req) < 11:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        try:
            username_receiver = req.split()[1]
        
            if not check_if_client_exists(username_receiver):
                client.send("11==USERNAME_DO_NOT_EXIST".encode('utf-8'))
            elif username_receiver == username_sender:
                client.send('27==CANNOT_STOP_PRIVATE_CONVERSATION_WITH_YOURSELF'.encode('utf-8'))
            elif not check_if_DM_sent(username_receiver, username_sender):
                client.send(f"30=={username_receiver}_DID_NOT_SEND_REQUEST".encode('utf-8'))
            else:
                username_receiver_index = usernames.index(username_receiver)
                client_receiver = clients[username_receiver_index]
                
                delete_private_conversation(username_receiver, username_sender)
                
                client_receiver.send(f'44==DM_DENIED;{username_sender}'.encode('utf-8'))
        except:
            client.send('16==MISSING_PARAMATERS'.encode('utf-8'))

def private_conversation_stop(client, req):
    if len(req) < 10:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        
        token = req.split()[1]
        user1, user2 = check_if_room_valid(username_sender, token)
        if user1: 
            delete_private_conversation(user1, user2)
            client.send(f'0==DM_STOPPED;{user2}'.encode('utf-8'))
            if user1 == username_sender:
                client_index = usernames.index(user2)
                client2 = clients[client_index]
                client2.send(f'47=={username_sender}'.encode('utf-8'))
            else:
                client_index = usernames.index(user1)
                client2 = clients[client_index]
                client2.send(f'47=={username_sender}'.encode('utf-8'))
        else: 
            client.send('15==NO_ROOM_ASSOCIATED_TO_TOKEN'.encode('utf-8'))

def send_message(client, req):
    if len(req) < 6:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    elif paused_clients.count(client) == 0:
        if req[-1] == '"':
            username_index = clients.index(client)
            client_username = usernames[username_index]
            # No token passed in request. ==> send public message
            msg = req[6:-1]
            # Send message to all connected clients.
            broadcast_msg(msg, client_username)
        else:
            # Token exist ==> private message
            token = req[-6:]
            msg = req[6:-8]
            # print(msg)
            username_index = clients.index(client)
            username_sender = usernames[username_index]
            
            user1, user2 = check_if_room_valid(username_sender, token)
            if user1: 
                pass
                client.send(f'40==1;{username_sender};"{msg}"'.encode('utf-8'))
                if user1 == username_sender:
                    client_index = usernames.index(user2)
                    client2 = clients[client_index]
                    client2.send(f'40==1;{username_sender};"{msg}"'.encode('utf-8'))
                else:
                    client_index = usernames.index(user1)
                    client2 = clients[client_index]
                    client2.send(f'40==1;{username_sender};"{msg}"'.encode('utf-8'))
            else:
                client.send('15==NO_ROOM_ASSOCIATED_TO_TOKEN'.encode('utf-8'))
    else:
        client.send('20==AFK_MODE'.encode('utf-8'))

def check_if_req_sent(username_sender, username_recv, filename):
    if FILE_TRANSFER.get(username_sender):
        return any(fl['receiver'] == username_recv and fl['filename'] == filename for fl in FILE_TRANSFER.get(username_sender))
    
    return False

def update_file_transfer_list(username_sender, username_recv, filename):
    for fl in FILE_TRANSFER.get(username_recv):
        if username_sender and filename in fl.values():
            FILE_TRANSFER[username_recv].remove(fl)
            break

def send_file_request(client, req):
    if req.count(' ') < 3:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        
        username_receiver = req.split()[1]
        filename = req.split()[2]

        if not check_if_client_exists(username_receiver):
            client.send("11==USERNAME_DO_NOT_EXIST".encode('utf-8'))
        elif username_receiver == username_sender:
            client.send('28==CANNOT_SEND_FILE_TO_YOURSELF'.encode('utf-8'))
        elif check_if_req_sent(username_sender, username_receiver, filename):
            client.send(f"29=={username_receiver}_REQUEST_ALREADY_SENT".encode('utf-8'))
        else:
            username_receiver_index = usernames.index(username_receiver)
            client_receiver = clients[username_receiver_index]
            
            if username_sender in FILE_TRANSFER:
                FILE_TRANSFER[username_sender].append({
                                                        "receiver": username_receiver,
                                                        "filename": filename
                                                    })
            else:
                FILE_TRANSFER[username_sender] = [{
                                                    "receiver": username_receiver,
                                                    "filename": filename
                                                }]
            
            size     = req.split()[3]
            client_receiver.send(f'42=={username_sender};{filename};{size}'.encode('utf-8'))
            client.send(f'0==SEND_FILE_REQUEST_SUCCESS'.encode('utf-8'))

def send_file_request_accepted(client, req):
    if req.count(' ') < 3:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        
        username_receiver = req.split()[1]
        filename = req.split()[2]
        if not check_if_client_exists(username_receiver):
            client.send("11==USERNAME_DO_NOT_EXIST".encode('utf-8'))
        elif username_receiver == username_sender:
            client.send('28==CANNOT_SEND_FILE_TO_YOURSELF'.encode('utf-8'))
        elif not check_if_req_sent(username_receiver, username_sender, filename):
            client.send(f"31=={username_receiver}_DID_NOT_SEND_REQUEST".encode('utf-8'))
        else:
            username_receiver_index = usernames.index(username_receiver)
            client_receiver = clients[username_receiver_index]
            
            update_file_transfer_list(username_sender, username_receiver, filename)
            
            addr = req.split()[3]
            client.send(f'0==OK_FILE_TRANSFER_ACCEPTED;{username_receiver};{filename}'.encode('utf-8'))
            client_receiver.send(f'45=={username_sender};{filename};{addr}'.encode('utf-8'))


def send_file_request_refused(client, req):
    if req.count(' ') < 2:
        client.send('16==MISSING_PARAMATERS'.encode('utf-8'))
    else:
        username_index = clients.index(client)
        username_sender = usernames[username_index]
        
        username_receiver = req.split()[1]
        filename = req.split()[2]
        if not check_if_client_exists(username_receiver):
            client.send("11==USERNAME_DO_NOT_EXIST".encode('utf-8'))
        elif username_receiver == username_sender:
            client.send('28==CANNOT_SEND_FILE_TO_YOURSELF'.encode('utf-8'))
        elif not check_if_req_sent(username_receiver, username_sender, filename):
            client.send(f"31=={username_receiver}_DID_NOT_SEND_REQUEST".encode('utf-8'))
        else:
            username_receiver_index = usernames.index(username_receiver)
            client_receiver = clients[username_receiver_index]
            
            update_file_transfer_list(username_sender, username_receiver, filename)
            
            client_receiver.send(f'46=={username_sender};{filename}'.encode('utf-8'))
            client.send(f'0==OK_FILE_TRANSFER_REFUSED;{username_receiver};{filename}'.encode('utf-8'))

# PAUSE A CLIENT
def pause_client(client):
    if not (client in paused_clients):
        paused_clients.append(client)
        client.send('0==AFK_OK'.encode('utf-8'))
    else:
        client.send('20==ALREADY_IN_AFK_MODE'.encode('utf-8'))

# RESUME a client who is in PAUSE mode
def resume_client(client):
    if client in paused_clients:
        paused_clients.remove(client)
        client.send('0==RESUME_OK'.encode('utf-8'))
    else:
        client.send('21==NOT_IN_AFK_MODE'.encode('utf-8'))

def unknown_command(client):
    client.send('32==UNKNOWN_COMMAND'.encode('utf-8'))

def handle_commands(message, client):
    command = message.decode('utf-8')
    
    if command == "/LIST":
        logger.info(f"Received request: \"/LIST\" from :{usernames[clients.index(client)]}")
        str_list = ';'.join(usernames)
        response = "0==LIST_" + str_list
        client.send(response.encode('utf-8'))
    elif command.startswith("/RENAME"):
        logger.info(f"Received request: \"/RENAME\" from :{usernames[clients.index(client)]}")
        rename_client(client, command)
    elif command == "/AFK":
        logger.info(f"Received request: \"/AFK\" from :{usernames[clients.index(client)]}")
        pause_client(client)
    elif command == "/RESUME":
        logger.info(f"Received request: \"/RESUME\" from :{usernames[clients.index(client)]}")
        resume_client(client)
    elif command.startswith("/DM") and command[:4] == "/DM ":
        logger.info(f"Received request: \"/DM\" from :{usernames[clients.index(client)]}")
        send_private_conversation_demand(client, command)
    elif command.startswith("/DM_ACCEPT"):
        logger.info(f"Received response: \"/DM_ACCEPT\" from :{usernames[clients.index(client)]}")
        private_conversation_accepted(client, command)
    elif command.startswith("/DM_DENY"):
        logger.info(f"Received response: \"/DM_DENY\" from :{usernames[clients.index(client)]}")
        private_conversation_refused(client, command)
    elif command.startswith("/DM_STOP"):
        logger.info(f"Received request: \"/DM_STOP\" from :{usernames[clients.index(client)]}")
        private_conversation_stop(client, command)
    elif command.startswith("/SEND_FILE"):
        logger.info(f"Received request: \"/SEND_FILE\" from :{usernames[clients.index(client)]}")
        send_file_request(client, command)
    elif command.startswith("/FILE_ACCEPT"):
        logger.info(f"Received response: \"/FILE_ACCEPT\" from :{usernames[clients.index(client)]}")
        send_file_request_accepted(client, command)
    elif command.startswith("/FILE_DENY"):
        logger.info(f"Received response: \"/FILE_DENY\" from :{usernames[clients.index(client)]}")
        send_file_request_refused(client, command)
    elif command.startswith("/MSG"):
        send_message(client, command)
    else:
        unknown_command(client)


def inform_new_client_connected(username):
    response = f'48=={username}'.encode('utf-8')
    for client in clients:
        client.send(response)

def inform_client_disconnected(username):
    response = f'49=={username}'.encode('utf-8')
    for client in clients:
        client.send(response)

# Function to deconnect a client
def deconnect_client(client):
    client_index = clients.index(client)
    clients.remove(client)
    client.close()
    username = usernames[client_index]
    usernames.remove(username)
    
    print(f"disconnected with {username}")
    logger.info(f"disconnected with {username}")
    
    inform_client_disconnected(username)
    send_list_clients(client)


# Function to handle the connection between the client and the server
def handle(client):
    while True:
        try:
            message = client.recv(BUFF_SIZE)
            handle_commands(message, client)
            
        except:
            deconnect_client(client)
            break

# Function to add a new client
def connect_new_client(client):
    while True:
        try:
            client.send('/USERNAME'.encode('utf-8'))
            
            username = client.recv(BUFF_SIZE).decode('utf-8')
            
            if not check_if_client_exists(username):
                usernames.append(username)
                clients.append(client)
                break
            else:
                client.send('14==USERNAME_ALREADY_EXIST'.encode('utf-8'))
        except:
            client.close()
            return
    
    print(f"Username of client is : {username}")
    logger.info(f"Username of client is : {username}")
    
    inform_new_client_connected(username)
    client.send('0==OK_CONNECTED'.encode('utf-8'))
    send_list_clients(client)
    
    # Create a thread to handle connection with all clients at the same time
    thread = threading.Thread(target=handle, args=(client, ))
    thread.start()


# Function responsble for receiving new clients connection
def receive():
    while True:
        try:
            client, address = server.accept()
            print(f"connected with {str(address)}")
            logger.info(f"connected with {str(address)}")
            
            # Create a thread to handle connection with all clients at the same time
            thread = threading.Thread(target=connect_new_client, args=(client, ))
            thread.start()
        except KeyboardInterrupt:
            break


# MAIN PROGRAM 
if __name__ == '__main__':
    receive()
    
    print("Stopping server...")
    logger.info("Stopping server...")
    
    for thread in threading.enumerate():
        if thread != threading.main_thread(): thread.join()
    
    server.close()
    sys.exit(0)
