import sys, time, os, random
import socket
import threading
import pyfiglet, click, emoji

from datetime import datetime
from colored import fg, bg, attr

nb_try = 0

username= None
not_allowed_characters= ['/', '\\', '@', '#', '&', '~', '&amp;', '&lt;', '&gt;', '=', '"', '_', '&', ' ']

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# HOST = '127.0.0.1'
HOST = socket.gethostbyname(socket.getfqdn(socket.gethostname()))
PORT = 6666
ADDR = (HOST, PORT)

users = []
pv_chat = {}
files_to_send = {}

emojis_list = ['alien', 'alien_monster', 'robot', 'boy', 'person', 'man', 'person_blond_hair', 'person_beard', 'superhero', 'supervillain', 'mage', 'genie']

# Constant storing size of buffer
BUFF_SIZE = 1024


def clear(): 
    # for windows 
    if os.name == 'nt': 
        _ = os.system('cls') 
    # for mac and linux
    else: 
        _ = os.system('clear') 

def set_console_size():
    cmd = 'mode 130, 35'
    os.system(cmd)

def intro_message():
    clear()
    time.sleep(0.2)
    
    welcome_msg = pyfiglet.figlet_format("WELCOME TO DNC CYBER CHAT", font = "slant")
    print(welcome_msg)
    conn_msg = pyfiglet.figlet_format("Connecting...", font = "digital")
    print(conn_msg)
    
    with click.progressbar(range(300000)) as bar:
        for i in bar:
            pass
        
    clear()
    time.sleep(0.2)

def get_avatar():
    return f":{random.choice(emojis_list)}:"

def help_manual():
    print(f"{bg('green')}#{attr(0)}" * 30, end=" ")
    print(emoji.emojize(f" -- :man_mechanic:{fg('sea_green_2')}HELP MANUAL{attr(0)}:woman_mechanic: --"), end=" ")
    print(f"{bg('green')}#{attr(0)}" * 30)
    
    print(f"# {fg('aquamarine_1a')}\"/AFK\"{attr(0)}: To enter to sleep mode.")
    print(f"# {fg('aquamarine_1a')}\"/RESUME\"{attr(0)}: To return to active mode. (No AFK)")
    print(f"# {fg('aquamarine_1a')}\"/RENAME [new username]\"{attr(0)}: To change your username.")
    print(f"# {fg('aquamarine_1a')}\"/LIST\"{attr(0)}: To get the list of connected users.")
    print(f"#")
    print(f"# {fg('aquamarine_1a')}\"/DM [username]\"{attr(0)}: To start a new private conversation with [username].")
    print(f"# {fg('aquamarine_1a')}\"/DM_ACCEPT [username]\"{attr(0)}: To accept private conversation request coming from [username].")
    print(f"# {fg('aquamarine_1a')}\"/DM_DENY [username]\"{attr(0)}: To refuse private conversation request coming from [username].")
    print(f"# {fg('aquamarine_1a')}\"/DM_STOP [username]\"{attr(0)}: To stop an existing private conversation with [username].")
    print(f"#")
    print(f"# {fg('aquamarine_1a')}\"/pv [username] [message]\"{attr(0)}: To send the message [message] to [username].")
    print(f"#")
    print(f"# {fg('aquamarine_1a')}\"/SEND_FILE [username] [file path]\"{attr(0)}: To send a file to [username].")
    print(f"# {fg('aquamarine_1a')}\"/FILE_ACCEPT [username] [filename]\"{attr(0)}: To accept the download of [filename] from [username].")
    print(f"# {fg('aquamarine_1a')}\"/FILE_DENY [username] [filename]\"{attr(0)}: To deny the download of [filename] from [username].")
    print("#")
    print(f"# {fg('dark_orange')} --> To send a public message just type the message and then press enter.{attr(0)}")
    print("#")
    print(f"{bg('green')}#{attr(0)}" * 90)

def get_current_time():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    return current_time

def print_users_list(users):
    print(f"{bg('grey_37')}[USERS LIST UPDATE]{attr(0)} {get_current_time()} :", end=' ')
    print(f"{bg('grey_37')}[", end=" ")
    for user in range(0, len(users) - 1):
        print(f"<@{users[user]}>,", end=" ")        
    
    print(f"<@{users[-1]}>", end=" ")
    print(f"]{attr(0)}")

def add_to_send_files_list(usern, path, size):
    if files_to_send.get(usern):
        files_to_send[usern].append((path, size))
    else:
        files_to_send[usern] = [(path, size)]

def receive_file(message):
    # message = "0==OK_FILE_TRANSFER_ACCEPTED;{username_receiver};{filename}"
    files_transfer_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    files_transfer_sock.bind(ADDR)
    files_transfer_sock.listen()
    
    filename = message.split(';')[2]
    filesize = int(get_path_of_file(message.split(';')[1], filename)[1])
    while True:
        try:
            conn, addr = files_transfer_sock.accept()
            file_data = conn.recv(BUFF_SIZE)
            total_data = len(file_data)
            
            f = open("new_"+filename, 'wb')
            f.write(file_data)
            while total_data < filesize:
                file_data = conn.recv(BUFF_SIZE)
                total_data += len(file_data)
                f.write(file_data)
            
            print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION]{attr(0)}: {bg('dark_orange')}{fg('black')}Completed Download of \"{filename}\" :check_mark: :check_mark: {attr(0)}"))
            conn.close()
            break
        except Exception as e:
            print(e)
            conn.close()
            break
    
    files_transfer_sock.close()

def send_file(address, path):
    send_file_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    send_file_sock.connect(address)
    
    try:
        with open(path, 'rb') as f:
            data_to_send = f.read(BUFF_SIZE)
            send_file_sock.send(data_to_send)
            while data_to_send != "":
                data_to_send = f.read(BUFF_SIZE)
                send_file_sock.send(data_to_send)
    except Exception as e:
        print(e)
    
    send_file_sock.close()

def get_path_of_file(user, filename):
    for fl in files_to_send.get(user):
        if os.path.basename(fl[0]) == filename:  return fl
    
    return None

def handle_file_send_accepted(message):
    # Exemple : message = {username_sender};{filename};{addr == 127.0.0.1:8000 (per example)}
    info = message.split(';')
    receiver_host = info[2].split(':')[0]
    receiver_port = info[2].split(':')[1]
    
    path_to_file = get_path_of_file(info[0], info[1])
    if path_to_file:
        sendFileThread = threading.Thread(target=send_file, args=((receiver_host, int(receiver_port)), path_to_file[0]))
        sendFileThread.start()

def handle_public_message(message):
    # 0;{username};"{message}"
    username_src_msg = message.split(';')[1]
    # text_message = message[8+len(username):-1]
    text_message = ' '.join(message.split('"')[1:])
    
    if username_src_msg == username:
        print(emoji.emojize(f"{bg('light_sky_blue_1')}{fg('black')}[MESSAGE :speech_balloon:]{attr(0)} {get_current_time()} {bg('yellow')}{fg('black')}<:vampire: @{username_src_msg}>{attr(0)}: {text_message}"))
    else:
        print(emoji.emojize(f"{bg('light_sky_blue_1')}{fg('black')}[MESSAGE :speech_balloon:]{attr(0)} {get_current_time()} {bg('purple_1a')}<{get_avatar()} @{username_src_msg}>{attr(0)}: {text_message}"))

def handle_private_message(message):
    username_src_msg = message.split(';')[1]
    text_message = ' '.join(message.split('"')[1:])
    
    if username_src_msg == username:
        print(emoji.emojize(f"{bg('chartreuse_4')}{fg('black')}[PRIVATE MESSAGE :speech_balloon: :locked:]{attr(0)} --> {get_current_time()} {bg('yellow')}{fg('black')}<:vampire: @{username_src_msg}>{attr(0)}: {text_message}"))
    else:
        print(emoji.emojize(f"{bg('chartreuse_4')}{fg('black')}[PRIVATE MESSAGE :speech_balloon: :locked:]{attr(0)} --> {get_current_time()} {bg('purple_1a')}<{get_avatar()} @{username_src_msg}>{attr(0)}: {text_message}"))

def handle_response(message):
    global users, username
    
    if message.startswith('0==LIST'):
        remove_code = message.split('_')[1]
        users = remove_code.split(';')
        print_users_list(users)
        
    elif message.startswith('0==DM_STOPPED'):
        pv_user = message.split(';')[1]
        if pv_chat.get(pv_user):
            del pv_chat[pv_user]
            print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Private conversation with {bg('purple_1a')}<@{pv_user}>{attr(0)} has been stopped."))
    
    elif message.startswith('0==AFK_OK'):
        print(emoji.emojize(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You are now in AFK Mode :crescent_moon:."))
        
    elif message.startswith('0==RESUME_OK'):
        print(emoji.emojize(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You are back to default mode (NO AFK) :hot_beverage:."))
    
    elif message.startswith('0==RENAME_OK'):
        username = message.split(';')[2]
        print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Username updated succefully. New username : {bg('yellow')}{fg('black')}<@{username}>{attr(0)}"))
    
    elif message.startswith('0==OK_FILE_TRANSFER_ACCEPTED'):
        recFileThread = threading.Thread(target=receive_file, args=(message,))
        recFileThread.start()
    
    elif message.startswith('10=='):
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Username not valid. Please retry a valid username!"))
    
    elif message.startswith('11=='):
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Username doesn't exist!"))
    
    elif message.startswith('15=='):
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: No room associated to the given data."))
    
    elif message.startswith('16=='):
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Not enough arguments!"))
    
    elif message.startswith('20=='):
        print(emoji.emojize(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You are in AFK :crescent_moon: Mode. Make sure to resume to send messages."))
    
    elif message.startswith('21=='):
        print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You are not in AFK Mode.")
    
    elif message.startswith('27=='):
        print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You Cannot send a private conversation command to yourself.")
    
    elif message.startswith('28=='):
        print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You Cannot send a file to yourself.")
    
    elif message.startswith('31==') or message.startswith('30=='):
        usern = message.split('==')[1].split('_')[0]
        print(f"{bg('red')}[WARNING]{attr(0)}: <@{usern}> didn't send you any request.")
    
    elif message.startswith('32=='):
        print(f"{bg('red')}[WARNING]{attr(0)}: Invalid command! (COMMANDS MUST BE IN UPPERCASE)")
    
    elif message.startswith('40=='):
        response = message.split('==')[1]
        if response.startswith('0'):
            handle_public_message(response)
        elif response.startswith('1'):
            handle_private_message(response)
    
    elif message.startswith('41=='):
        user_request = message.split(';')[1]
        print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Request for private conversation from {bg('purple_1a')}<@{user_request}>{attr(0)}"))
    
    elif message.startswith('42=='):
        req_data = message.split('==')[1]
        user_request = req_data.split(';')[0]
        filename = req_data.split(';')[1]
        size = req_data.split(';')[2]
        add_to_send_files_list(user_request, filename, size)
        print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Request to send you the file :page_facing_up: {fg('dark_orange')}\"{filename}\"{attr(0)} with a size of {fg('dark_orange')}\"{size}\"{attr(0)} Bytes from {bg('purple_1a')}<@{user_request}>{attr(0)}"))
    
    elif message.startswith('43=='):
        token = message.split('==')[1].split(';')[0]
        pv_user = message.split(';')[1]
        pv_chat[pv_user] = token
        print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Private conversation started with {bg('purple_1a')}<@{pv_user}>{attr(0)}"))
    
    elif message.startswith("44=="):
        pv_user = message.split(';')[1]
        print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: {bg('purple_1a')}<@{pv_user}>{attr(0)} refused your request for private conversation."))
    
    elif message.startswith('47=='):
        pv_user = message.split('==')[1]
        if pv_chat.get(pv_user):
            del pv_chat[pv_user]
            print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Private conversation with {bg('purple_1a')}<@{pv_user}>{attr(0)} has been stopped."))
    
    elif message.startswith('45=='):
        handle_file_send_accepted(message.split('==')[1])
    
    elif message.startswith('46=='):
        usern = message.split('==')[1].split(';')[0]
        filename = message.split(';')[1]
        print(emoji.emojize(f"{bg('light_steel_blue')}{fg('black')}[INFORMATION :blue_circle:]{attr(0)}: Send file ({filename}) request has been refused by {bg('purple_1a')}<@{usern}>{attr(0)}."))

    
    elif message.startswith('48=='):
        username_src_msg = message.split('==')[1]
        print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: {get_current_time()} {bg('purple_1a')}<@{username_src_msg}>{attr(0)} has joined the chat.")
    
    elif message.startswith('49=='):
        username_src_msg = message.split('==')[1]
        print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: {get_current_time()} {bg('purple_1a')}<@{username_src_msg}>{attr(0)} has left the chat.")
    
    elif message.startswith('50=='):
        old_username = message.split('==')[1].split(';')[0]
        new_username = message.split(';')[1]
        print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: {bg('purple_1a')}<@{old_username}>{attr(0)} changed to {bg('purple_1a')}<@{new_username}>{attr(0)}")

    # else:
        # print(message)

# Function to handle messages comming from the server
def receive():
    global username
    while True:
        try:
            message = client.recv(BUFF_SIZE).decode('utf-8')
            handle_response(message)
        except:
            print(f"{bg('red')}[PROBLEM]: An error occurred !{attr(0)}")
            client.close()
            sys.exit(0)

def send_private_msg(message):
    try: 
        userToReceive = message.split()[1]
        if pv_chat.get(userToReceive):
            token = pv_chat.get(userToReceive)
            msg_data = message[(5+len(userToReceive)):]
            client.send(f'/MSG "{msg_data}" {token}'.encode('utf-8'))
        else:
            print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: Send DM request to start a conversation with {bg('purple_1a')}<@{userToReceive}>{attr(0)}.")
    except:
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Please provide valid data."))

def stop_private_conversation(message):
    try: 
        userToReceive = message.split()[1]
        if pv_chat.get(userToReceive):
            token = pv_chat.get(userToReceive)
            client.send(f'/DM_STOP {token}'.encode('utf-8'))
        else:
            print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You don't have any private conversation with {bg('purple_1a')}<@{userToReceive}>{attr(0)}.")
    except:
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Please provide valid data."))

def send_file_request(message):
    try: 
        usern = message.split()[1]
        path = message.split()[2]
        if os.path.isfile(path):
            filename = os.path.basename(path)
            size = str(os.path.getsize(filename))
            add_to_send_files_list(usern, path, size)
            req = f"/SEND_FILE {usern} {filename} {size}"
            client.send(req.encode('utf-8'))
        else:
            print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: File do not exist. Please make sure of your information."))
    except:
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Please provide valid data."))

def send_file_accept_request(message):
    # message result = /FILE_ACCEPT username filename port
    try: 
        usern = message.split()[1]
        filename = message.split()[2]
        req = f"/FILE_ACCEPT {usern} {filename} {HOST}:{PORT}"
        client.send(req.encode('utf-8'))
    except:
        print(emoji.emojize(f"{bg('red')}[WARNING :red_circle:]{attr(0)}: Please provide valid data."))

def write():
    global username
    
    paused = False
    
    while True:
        try:
            message = input("")
            
            if message.lower() == '/help':
                help_manual()
                continue
            
            if message.startswith('/RESUME'):
                paused = False
                client.send(message.encode('utf-8'))
                continue
            
            if paused:
                print(f"{bg('dark_cyan')}[NOTICE]{attr(0)}: You are in AFK Mode. Make sure to resume to send messages.")
            
            if message.startswith('/AFK'):
                paused = True
                client.send(message.encode('utf-8'))
            
            if not paused:
                if message.startswith('/pv'):
                    send_private_msg(message)
                elif message.startswith('/'):
                    if message.startswith('/DM_STOP'):
                        stop_private_conversation(message)
                        continue

                    if message.startswith('/SEND_FILE'):
                        send_file_request(message)
                        continue
                    
                    if message.startswith('/FILE_ACCEPT'):
                        send_file_accept_request(message)
                        continue
                    
                    client.send(message.encode('utf-8'))
                else:
                    # msg = f'{username}: {message}'
                    client.send(f'/MSG "{message}"'.encode('utf-8'))
            
            # time.sleep(0.2)
        except KeyboardInterrupt:
            break

def validate_username(username):
    return any(character in username for character in not_allowed_characters)

def connect():
    global username
    
    while True:
        try:
            message = client.recv(BUFF_SIZE).decode('utf-8')
            if message == "/USERNAME":
                # Choosing client username
                while True:
                    username = input("Choose a username: ")
                    if not validate_username(username):
                        client.send(username.encode('utf-8'))
                        break
                    else:
                        print(f"{fg('red')}Not valid. Please retry.{attr(0)}")
            elif message == "14==USERNAME_ALREADY_EXIST":
                print(f"{fg('red')}Username already exists.{attr(0)}")
                
            elif message == "0==OK_CONNECTED":
                print(emoji.emojize(f"{fg('green')}Connected to the server{attr(0)} :grinning_face_with_smiling_eyes:."))
                print(emoji.emojize("Enjoy your chat experiences :smiling_face_with_sunglasses:."))
                print(emoji.emojize(f"{fg('aquamarine_1a')}Type: \"/help\" if you want help instructions :face_with_monocle:.{attr(0)}"))
                receive_thread = threading.Thread(target=receive)
                receive_thread.start()
                
                write_thread = threading.Thread(target=write)
                write_thread.start()
                
                break
        except:
            print(f"{fg('red')}An error occurred !{attr(0)}")
            client.close()
            break

if __name__ == '__main__':

    while True:
        try:
            SERVER_HOST = input("Enter address: ")
            # client.connect(('127.0.0.1', 5555))
            client.connect((SERVER_HOST, 5555))
            break
        except:
            print(f"{fg('red')}Error. Please Provide a valid address!.{attr(0)}")
            nb_try += 1
            if (nb_try == 3):
                sys.exit(1)

    intro_message()
    
    connect_thread = threading.Thread(target=connect)
    connect_thread.start()
    
    for thread in threading.enumerate():
        if thread != threading.main_thread(): thread.join()

    sys.exit(0)