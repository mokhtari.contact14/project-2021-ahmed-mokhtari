import { LOGIN_ACTION, LOGOUT_ACTION, SAVE_DECK_ACTION } from './actions'

const initialState = { isLogged: false, username: undefined, token: undefined, lastSavedDeck: [] };
// if connected, then { isLogged: true, username: "...", token: "..."}

export const connexionReducer = (state=initialState, action) => {
    switch(action.type) {
        case LOGIN_ACTION:
            return {...state, isLogged: true, username: action.username, token: action.token }
        case LOGOUT_ACTION:
            return {...state, isLogged: false, username: undefined, token: undefined }
        case SAVE_DECK_ACTION:
            return {...state, lastSavedDeck: action.deck}
        default:
            return state;
    }
}