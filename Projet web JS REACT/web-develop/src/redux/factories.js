import { LOGIN_ACTION, LOGOUT_ACTION, SAVE_DECK_ACTION } from './actions'

export const loginAction = (pUsername, pToken) => {return { type: LOGIN_ACTION, username: pUsername, token: pToken }};
export const logoutAction = () => { return { type: LOGOUT_ACTION }}
export const saveDeckAction = (pDeck) => { return { type: SAVE_DECK_ACTION, deck: [...pDeck] }}