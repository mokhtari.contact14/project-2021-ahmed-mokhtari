import React from 'react'
import './Footer.css'
import { Redirect, Link } from 'react-router-dom'

function Footer (props) {
    return <div className="footer">
        <p><Link to ="/seeMore">A propos</Link></p>
        <p><Link to ="/contact">Contactez-nous</Link></p>
        <p>&#169;League of Sparta</p>
    </div>
}

export default Footer;