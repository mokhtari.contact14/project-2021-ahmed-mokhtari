import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { logoutAction } from '../../../redux/factories';
import { useHistory } from 'react-router-dom';
import { userLogout } from '../../../webservice/endpoints';
import logo from '../../../assets/helmet.png';
import './NavBar.css';

function NavBar() {
  const [showMenu, setShowMenu] = useState(false);
  const username = useSelector(state => state.username)
  const isConnected = useSelector(state => state.isLogged)
  const dispatch = useDispatch();
  const userToken = useSelector(state => state.token)
  const history = useHistory();

  // #region functions 
  const showMobileMenu = () => {
    setShowMenu(!showMenu)
  }

  /**
   * function used to delete the session data after logout
   */
  const removeSession = () => {
    sessionStorage.removeItem('name')
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('isLogged')
    sessionStorage.removeItem('lastSavedDeck')
  }

  /**
   * perform logout action
   * @param {string} token 
   */
  const logout = (token) => {
    userLogout(token)
        .then((rq) => {
            if(rq.status === 200) {
                dispatch(logoutAction())
                removeSession()
                history.push("/signIn")
            } else {
              console.log("Error while disconnecting")
            }
        });
  }

  // #endregion
  // const leftNavItem = [{"label": "Creer son deck", "url": "/createDeck", }]

  const notlogged_navbar_items = [{"label": "Connexion", "redirect": "/signIn"}, {"label": "Inscription", "redirect": "/signUp"}]
  const notlogged_navbar_items_burger = [...notlogged_navbar_items,{"label": "A propos", "redirect": "/seeMore"}, {"label": "Contact", "redirect": "/contact"}]
  
  const burger_menu_icon = <label id="burger-icon" onClick={() => { showMobileMenu() }}>
                            <span></span>
                            <span></span>
                            <span></span>
                          </label>

  const burger_menu = 
                      <div className="burger_menu_displayed">
                      {!isConnected 
                        ? notlogged_navbar_items_burger.map((item, index) => 
                        <span key={index} className="mobile-label" onClick={() => { history.push(item.redirect); showMobileMenu() } }>
                          {item.label}
                        </span>) 
                        :
                        <span className="mobile-label" onClick={() => { logout(userToken); showMobileMenu() } }>
                          Déconnexion
                        </span>
                      }
                      </div>

  const notLoggedRightPart =  <div className="navbar-nav ml-auto">
                                {burger_menu_icon}
                                {notlogged_navbar_items.map((item, index) => <button key={index} className="custom-button" onClick={() => history.push(item.redirect)}>{item.label}</button>)}
                              </div>

  const loggedRightPart = <div className="navbar-nav ml-auto">
    <div className="display_user">
      <h4 className="username">{username}</h4>
    </div>
    <button id="disconnect" className="custom-button" onClick={() => {logout(userToken);}}>Déconnexion</button>
      {burger_menu_icon}
    </div>

  return (
    <nav className="custom-nav-bar">
      <div className="navbar navbar-expand-lg">
          <div className="navbar-brand">
            <img className="logo" src={logo} alt="" width="30" height="30"/>
            <h2><Link key="homepage" className="text-white-50 nav_title" to="/">League Of Spartans</Link></h2>
          </div>
          {isConnected ? loggedRightPart : notLoggedRightPart }
      </div>
      {showMenu ? burger_menu : null}
    </nav>
  );
}

export default NavBar;
