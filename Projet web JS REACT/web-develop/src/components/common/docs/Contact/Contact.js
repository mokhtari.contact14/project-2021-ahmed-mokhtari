import React from 'react'
import './Contact.css'
import { Redirect, Link } from 'react-router-dom'
import profileAdrien from '../images/profileAdrien.jpg'
import profileAmbasse from '../images/profileAmbasse.jpg'
import profileMahdi from '../images/profileMahdi.png'
import profileAhmed from '../images/profileAhmed.jpg'
import profileYannis from '../images/profileYannis.jpg'

function Contact () {
    return <div className="contactInfos">
                <h1 className ="titleContact">Votre équipe est là pour vous aidez...</h1>
                <section className="contact">
                    <section className="onePerson">
                        <h2 className="titlePerson">Gestion du projet</h2>
                        <article className = "infoPerson">
                            <img className ="profilePerson" alt="photoMahdi" src={profileMahdi}/>
                            <h3 className ="namePerson">Mahdi Lektati</h3>
                            <p className="contactPerson">email: <span>lektati1@gmail.com</span></p>
                            <p className = "paraPerson">
                                Définir le contexte, les enjeux, les objectifs techniques ainsi que les livrables et les axes de développement envisagés
                            </p>
                        </article>
                    </section>
                    <section className="onePerson">
                        <h2 className="titlePerson">Architecte</h2>
                        <article className = "infoPerson">
                            <img className ="profilePerson" alt="photoAdrien" src={profileAdrien}/>
                            <h3 className ="namePerson">Adrien Bruni</h3>
                            <p className="contactPerson">email: <span>adrien@bruni.fr</span></p>
                            <p className = "paraPerson">Aider les membres de l’équipe à utiliser le webservice donné à l’aide de multiples fonctions JS, permettant d’utiliser tout cela sans se soucier des headers, paramètres.</p>
                        </article>
                    </section>
                    <section className="onePerson">
                        <h2 className="titlePerson">Designer</h2>
                        <article className = "infoPerson">
                            <img className ="profilePerson" alt="photoAmbasse" src={profileAmbasse}/>
                            <h3 className ="namePerson">Ambasse Djabiri</h3>
                            <p className="contactPerson">email: <span>ambass.d@outlook.com</span></p>
                            <p className = "paraPerson">Accompagner l’équipe en leur proposant des maquettes avec un style qui est uniforme et en lien avec le jeu.</p>
                        </article>
                    </section>
                    <section className="onePerson">
                        <h2 className="titlePerson">Responsable Qualité</h2>
                        <article className = "infoPerson">
                            <img className ="profilePerson" alt="photoAhmed" src={profileAhmed}/>
                            <h3 className ="namePerson">Ahmed Mokhtari</h3>
                            <p className="contactPerson">email: <span>mokhtari.contact14@gmail.com</span></p> 
                            <p className = "paraPerson">Communiquer avec les membres d'équipe sur les objectifs et les critères de qualité retenus et l’organisation du code</p>
                        </article>
                    </section>
                    <section className="onePerson">
                        <h2 className="titlePerson">Responsable Mobile</h2>
                        <article className = "infoPerson">
                            <img  className ="profilePerson" alt="photoYannis" src={profileYannis}/>
                            <h3 className ="namePerson">Yannis Benhenda</h3>
                            <p className="contactPerson">gitLab: <span>PoutreLaLoutre</span></p>
                            <p className = "paraPerson">Bla blabla bla........................sczds x.dsx dscdzs</p>
                        </article>
                    </section>
                </section>
  
            </div>
}

export default Contact;