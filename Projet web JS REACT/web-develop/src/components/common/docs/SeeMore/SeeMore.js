import React, { useState } from 'react';
import './SeeMore.css';

function seeMore(){
    return (
                <div className="apropos">
                    <h1 className = "titleApropos" >A propos</h1>
                    <section className = "infosApropos">
                        <h2 className="titleApropos">LEAGUE OF SPARTANS</h2>
                        <article className = "textApropos">
                            <p className = "paraApropos">League of Sparta est une application Web de jeu vidéo basé sur le jeu League of Stone. C’est un jeu tiré d’une idée de deux enseignantes à l’université Jean Jaurès, il est question de développer le système de jeu Hearthsone développé par Blizzard et les données ouvertes de League of Legends développé par Riot Games. Un groupe d’étudiants sous le nom de League of Sparta vous propose une implémentation du jeu League of Stone. Sparta (Sparte) est la fille du dieu fleuve Eurotas en Grece. Elle se marie avec Lacédémon, de qui elle a un fils nommé Amyclas, ainsi  en l’honneur de sa femme,  Lacedemon fondera la ville de Sparta.</p>
                        </article>                        
                    </section>
                    <section className = "infosApropos">
                        <h2 className="titleApropos">MODE DE JEU</h2>
                        <article className = "textApropos">
                            <p className = "paraApropos"> 
                                Les deux joueurs s’affrontant possèdent un deck 3 de 20 cartes et 150 points de vie. Chaque
                                carte décrit un champion avec une statistique d’attaque et une de défense. Les joueurs jouent
                                chacun leur tour dans l’objectif de réduire les points de vie de l’adversaire à 0 pour gagner la
                                partie. Au début de la partie, les deux joueurs piochent 4 cartes. Durant son tour, un joueur peut
                                effectuer 3 actions dans l’ordre qu’il le souhaite :
                            </p>
                            <ul className = "ulApropos">
                                <li className = "liApropos">Piocher une carte (une fois par tour)</li>
                                <li className = "liApropos">Poser une carte sur le plateau (au maximum 5 par joueur sur le plateau)</li>
                                <li className = "liApropos">Attaquer (une fois par carte sur le plateau)</li>
                            </ul>
                            <p className = "paraApropos">
                                Une carte posée sur le plateau durant ce tour ne pourra attaquer qu’au tour suivant. Au tour
                                suivant la carte peut attaquer un monstre sur le plateau adverse. Résolution de l’attaque :
                            </p>
                            <ul className = "ulApropos">
                                <li className = "liApropos">Si la valeur d’attaque de la carte qui attaque est supérieure à la valeur de défense de la
                                carte adverse alors cette dernière est supprimée et la différence entre les deux valeurs
                                de carte est retirée aux points de vie de l’adversaire.</li>
                                <li className = "liApropos">Si les deux cartes ont une attaque et une défense égales alors les deux cartes sont détruites.</li>
                                <li className = "liApropos">Si la valeur d’attaque est inférieure à la valeur de défense alors la carte attaquante est détruite.</li>
                                <li className = "liApropos">S’il n’y a aucune carte sur le plateau adverse alors la carte peut directement attaquer les points de vie de l’adversaire.</li>
                            </ul>

                            <p className = "paraApropos">Une fois qu’une carte a attaqué, elle devra attendre le tour suivant pour attaquer de nouveau.</p>
                        </article>
                    </section>
                </div> 
   
   )}

export default seeMore