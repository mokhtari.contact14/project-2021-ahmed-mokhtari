import React, { useEffect, useState } from "react";
import Card from "../../shared/card/Card.js";
import { cardsGetAll } from "../../../webservice/endpoints"
import './Deck.css'
import { useDispatch, useSelector } from "react-redux";
import { saveDeckAction } from "../../../redux/factories.js";
import { useHistory } from 'react-router-dom';

function Deck(props) {
    const token = useSelector(state => state.token)
    const lastSavedUserDeck = useSelector(state => state.lastSavedDeck)
    const [selectedChampions, setSelectedChampions] = useState(lastSavedUserDeck)
    const [availableChampions, setAvailableChampions] = useState([])
    const [isDeckValidated, setIsDeckValidated] = useState(lastSavedUserDeck.length !== 0)
    const [isDeckFull, setIsDeckFull] = useState(false)
    const history = useHistory();
    const dispatch = useDispatch()

    const CHAMPION_PER_DECK = 20;
    /**
     * Hook to get all cards available
     */
    useEffect(() => {
        if(availableChampions.length === 0)
            cardsGetAll(token)
            .then((rqResult) => rqResult.json())
            .then((data) => {
                /**
                 * Check for each ID in retrieved data if an id already in selected deck
                 * Filter concerned one to do not print those
                 */
                const currentDeck = lastSavedUserDeck.map(item => item._id)
                const unselectedChampions = data.filter((item) => !currentDeck.includes(item._id))
                setAvailableChampions(unselectedChampions)
            });
    });

    function placeChampionToAvailable(champion) {
        setSelectedChampions((selectedChampions.filter((champ) => champ !== champion)))
        setAvailableChampions([...availableChampions, champion])
    }

    function placeChampionToSelected(champion) {
        if(selectedChampions.length >= CHAMPION_PER_DECK) {
            setIsDeckFull(true);
            return
        } 
        setAvailableChampions(availableChampions.filter((champ) => champ !== champion))
        setSelectedChampions([...selectedChampions, champion])
    }

    const convert_array_to_string = () => {
        return JSON.stringify(selectedChampions)
    }

    function onValidateDeck() {
        // if(window.confirm("Vous avez choisi : " + selectedChampions.map((champ) => champ.name) + ". Êtes-vous sûr ?")) {
        dispatch(saveDeckAction(selectedChampions))
        sessionStorage.setItem('lastSavedDeck', convert_array_to_string())
        setIsDeckValidated(true)
        setIsDeckFull(false);
        // }
    }

    function onModifyDeck() {
        setIsDeckValidated(false)
    }
    
    const confirmed_buttons =   <div>
                                    <button className="otherButton" onClick={onModifyDeck}> Modifier</button>
                                    <button className="otherButton" onClick={() => history.push("/")}> Go Back</button>
                                </div>

    return (
        <section className="deck">
            <section className="available_cards">
                <div className="inline-items">
                    <h2 className="card-container-title">Cartes disponibles</h2>
                    {/* TODO: FILTERS ? */}
                </div>
                <div className="card-container">
                    {availableChampions.map((champion, _) => <Card key={champion.id} champion={champion} onClickCallback={!isDeckValidated ? () => placeChampionToSelected(champion) : () => {}}/>)}
                </div>
            </section>
            <section className="current_deck">
                <div className="inline-items">
                    <h2 className="card-container-title">Votre deck ({selectedChampions.length}/{CHAMPION_PER_DECK})</h2>
                    { isDeckValidated ? confirmed_buttons : <button className="otherButton" disabled={selectedChampions.length !== 20} onClick={() => onValidateDeck()}>Valider</button>}
                </div>

                {isDeckFull ? <div className="msg-full-deck"><h3>Vous avez déjà le nombre maximum de champion dans votre deck (20 champions)</h3></div>: ''}
                
                <div className="card-container">
                    {selectedChampions.map((champion, _) => <Card key={champion.id} champion={champion} onClickCallback={!isDeckValidated ? () => placeChampionToAvailable(champion) : () => {}}/>)}
                </div>
            </section>
        </section>
    );
}

export default Deck;
