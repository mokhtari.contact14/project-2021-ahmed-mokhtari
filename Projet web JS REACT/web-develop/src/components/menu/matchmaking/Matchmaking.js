import React, { useState, useEffect } from 'react'
import ReceivedRequest from './RequestReceived/RequestReceived'
import PendingRequests from './PendingRequest/PendingRequest'
import { useSelector } from 'react-redux'
import './Matchmaking.css'
import { useHistory } from 'react-router-dom'
import { matchmkGetAllAvailable, matchmkGoInQueue, matchGetCurrentGame, matchmkGoOutOfQueue, matchmkAcceptRequest, matchmkRequest, matchInitDeck } from '../../../webservice/endpoints'
import Loader from '../../shared/loader/Loader'


function Matchmaking(props) {

    // If true show pending request, else requests
    const [showPendingRequest, setShowPendingRequest] = useState(true)
    const [pendingRequest, setPendingRequest] = useState([])
    const [receivedRequest, setReceivedRequest] = useState([]);
    const [requestedMatchmkId, setRequestedMatchmkId] = useState(undefined); // Undefined: no data, string: an id
    const disableButton = false
    const [showLoader, setShowLoader] = useState(false);

    const history = useHistory()
    const token = useSelector(state => state.token)
    const userDeck = useSelector(state => state.lastSavedDeck)
    
    const refresh = () => {
        // * This one retrieve persons who are waiting for a match
        matchmkGetAllAvailable(token)
        .then(rq => rq.json())
        .then(rq => setPendingRequest(rq), err => console.log(err))
        // * This one retrieve requests
        matchmkGoInQueue(token)
        .then(rq => rq.json())
        .then(rq => {setReceivedRequest(rq.request)}, err => console.log(err))
    }

    const quit = () => {
        setShowLoader(true)
        matchmkGoOutOfQueue(token)
        .then(() => {
            history.push("/")
        })
    }

    const goToMatchPage = () => {
        
    }

    const switchMode = () => {
        setShowPendingRequest(!showPendingRequest)
    }

    const setCheckingIfAcceptedInterval = () => {
        const id = setInterval(() => {
            matchmkGetAllAvailable(token)
            .then(rq => rq.json())
            .then(rq => {
                const userStillPresent = rq.map(item => item.matchmakingId).includes(requestedMatchmkId)
                if(!userStillPresent) {
                    matchGetCurrentGame(token)
                    .then(rq => rq.json())
                    .then(data => {
                        clearInterval(id)
                        setTimeout(() => {
                            matchInitDeck(userDeck, token)
                            .then(rq => {
                                if(rq.ok) {
                                    console.log(rq.json())
                                    history.push("/game")
                                } else {
                                    console.log(rq)
                                }
                            })
                        }, 500)
                        // setRequestedMatchmkId(undefined)
                        // console.log("SUCCESS")
                    },
                    err => {
                        /* If errer happenned, still no match associated */
                        // * Meaning he declined
                        clearInterval(id)
                        setRequestedMatchmkId(undefined)
                        setShowLoader(false)
                    })
                } else {
                    // Nothing happen
                }
            })
        }, 2000)
        return id
    }
    
    const setStandardRefreshInterval = () => {
        refresh()
        return setInterval(refresh, 5000)
    }

    const pendingButtonCallback = (matchmakingInstance) => {
        setShowLoader(true)
        matchmkRequest(matchmakingInstance.matchmakingId, token)
        .then(rq => {
            if(rq.ok) {
                setRequestedMatchmkId(matchmakingInstance.matchmakingId)
            } else {
                console.log("error")
            }
        })
    }

    const acceptButtonCallback = (matchmakingInstance) => {
        setShowLoader(true)
        matchmkAcceptRequest(matchmakingInstance.matchmakingId, token)
        .then(rq => {
            if(rq.ok) {
                setTimeout(() => {
                    matchInitDeck(userDeck, token)
                    .then(rq => {
                        if(rq.ok) {
                            console.log(rq.json())
                            history.push("/game")
                        } else {
                            console.log(rq)
                        }
                    })
                }, 2000)
                
                
            } else {
                refresh()
                setShowLoader(false)
            }
        })
        /**
         * Redirect to the page of the game
         */
    }

    useEffect(() => {
        let id = undefined;
        if(requestedMatchmkId === undefined) {
            id = setStandardRefreshInterval()
        }
        else {
            id = setCheckingIfAcceptedInterval()
        }
        return () => {
            clearInterval(id)
        }
    }, [requestedMatchmkId])

    if(showLoader) {
        return <Loader/>
    }
    return <section className="matchmaking-frame">
        { showPendingRequest ? <PendingRequests 
        requests={pendingRequest}
        onClickCallback={pendingButtonCallback}/>
        : 
        <ReceivedRequest 
        requests={receivedRequest}
        onClickCallback={acceptButtonCallback}
        />}

        <div className="button-group">
            <button className="custom-button" disabled={disableButton} onClick={switchMode}>{ showPendingRequest ? `Demandes reçues (${receivedRequest.length})`  : `Requêtes de match (${pendingRequest.length})` }</button>
            <button className="custom-button" disabled={disableButton} onClick={refresh}>Rafraîchir</button>
            <button className="custom-button" disabled={disableButton} onClick={quit}>Quitter le lobby</button>
        </div>
    </section>
}

export default Matchmaking