import React, { useState } from 'react'
import PlayerListItem from '../player_list_item/PlayerListItem'


function ReceivedRequest(props) {
    const callback = (item) => {
        console.log("matchmaking/acceptRequest")
        props.onClickCallback(item)
    }

    return <section className="item-selection">
        {props.requests.map((item, index) => <PlayerListItem key={index} username={item.name} 
        textContent="aimerais te rejoindre" buttonContent="Accepter" 
        onClickCallback={() => {callback(item)}}/>
    )}
    </section>
}

export default ReceivedRequest