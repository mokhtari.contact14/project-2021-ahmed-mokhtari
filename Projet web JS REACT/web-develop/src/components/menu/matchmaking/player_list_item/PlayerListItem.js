import React from 'react'
import './PlayerListItem.css'

function PlayerListItem(props) {
    return <div className="player-list-item">
        <p className="text-item">
            {props.username} {props.textContent}
        </p>
        <button className="custom-button" onClick={props.onClickCallback}>{props.buttonContent}</button>
    </div>
}

export default PlayerListItem