import React, { useEffect, useState } from 'react'
import PlayerListItem from '../player_list_item/PlayerListItem'

function PendingRequests(props) {
    const callback = (item) => {
        console.log("matchmaking/request")
        props.onClickCallback(item)
    }

    return <section className="item-selection">
    {props.requests.map((item, index) => <PlayerListItem key={index} username={item.name} 
        textContent="attend un adversaire ... " buttonContent="Jouer" 
        onClickCallback={() => {callback(item)}}/>)
    }
    </section>
}

export default PendingRequests