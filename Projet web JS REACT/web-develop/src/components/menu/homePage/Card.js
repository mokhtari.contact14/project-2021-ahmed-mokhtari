import React from 'react';
import './HomePage.css';

function Card(props) {
    return (
        <div className="card">
            <img src={props.src_image} className="rounded card-img-top" alt={props.src_image} />
        </div>
    );
}

export default Card;