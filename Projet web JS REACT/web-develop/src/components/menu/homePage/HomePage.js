import React from 'react';
import './HomePage.css';
import Carousel from 'react-elastic-carousel';
import { Link } from 'react-router-dom';
import Card from './Card';
import img1 from './Images/Bg1.jpg';
import img2 from './Images/Bg2.jpg';
import img3 from './Images/Bg3.jpg';
import img4 from './Images/Bg4.jpg';
import img5 from './Images/Bg5.jpg';
import img6 from './Images/Bg6.png';
import img7 from './Images/Bg7.jpg';

function HomePage(props) {

    const breakPoints = [
        {width: 1, itemsToShow: 1},
        {width: 500, itemsToShow: 2},
        {width: 768, itemsToShow: 3},
        {width: 1500, itemsToShow: 4},
    ]
    
    const cards = [
        img1,
        img2,
        img3,
        img4,
        img5,
        img6,
        img7,
    ]
    
    return (
        <div className="home-page">
            <h1 className="welcome-message">Bienvenue dans <br /> <span>LEGENDS OF SPARTA</span></h1>
            <button className="otherButton btn btn-gradient homepage-link">
                <Link key="homepage" to="/signIn"> Commencer à jouer </Link>
            </button>
            <Carousel 
                breakPoints={breakPoints} 
                enableAutoPlay
            >
                {cards.map((img, index) => <Card key={index} src_image={img} />)}
            </Carousel>
        </div>
    );
}

export default HomePage;