import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from 'react-router-dom';
import { matchGetCurrentGame } from "../../../webservice/endpoints";
import './HomeLogged.css';

function HomeLogged(){
    const loggedUsername = useSelector(state => state.username);
    const history = useHistory();
    const lastSavedUserDeck = useSelector(state => state.lastSavedDeck)

    const UNSUBSCRIBE_URL = "/unsubscribe";
    const token = useSelector(state => state.token)

    // redirect to the match if there was one
    useEffect(() => {
        matchGetCurrentGame(token)
        .then(rq => {
            if(rq.ok) history.push("/game")
            else{}},
            (err) => {})
        
    }, [])
    

    return(
        <div  id="content">
            <h1 id="titre">Bienvenue {loggedUsername} !</h1> 
            <button id="jouer" onClick={ lastSavedUserDeck.length !== 0 ? () => history.push("/matchmaking") : () => history.push("/createDeck")} className="otherButton" type="button" value="Jouer">
                Jouer
            </button>
            <button id="deck" onClick={() => history.push("/createDeck")} className="otherButton" type="button" value="Deck">
                Deck
            </button>
            <button id="Supprimer" onClick={() => history.push(UNSUBSCRIBE_URL)} className="otherButton" type="button" value="Se désabonner">
                Supprimer compte
            </button>
        </div>
    );
    
}

export default HomeLogged;