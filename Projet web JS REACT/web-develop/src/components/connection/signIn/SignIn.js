import React, { useState } from 'react';
import { Form, Field } from "react-final-form";
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginAction } from '../../../redux/factories';
import { errorSpan } from '../common';
import { useHistory } from 'react-router-dom';
import './SignIn.css';
import { userLogin } from '../../../webservice/endpoints';
import imgConnexion from '../images/Connexion.jpg';

function SignIn(props) {
    const [idNotProvidedError, setIdNotProvidedError] = useState(null);
    const [passwordNotProvidedError, setPasswordNotProvidedError] = useState(null);
    const [passwordOrIdIncorrect, setPasswordOrIdIncorrect] = useState(null);
    const [isCreateButtonClicked, setIsCreateButtonClicked] = useState(false);
    const history = useHistory();

    const dispatch = useDispatch();

    /**
     * creates a session that store user login details
     * @param {String} name 
     * @param {String} token 
     */
    const createSession = (name, token) => {
        sessionStorage.setItem('name', name)
        sessionStorage.setItem('token', token)
        sessionStorage.setItem('isLogged', true)
    }


    /**
     * send request to the server to submit login action
     * @param {Object} values 
     */
    const handleSumbit = (values) => {
        if (values.pseudo === undefined) setIdNotProvidedError(true); else setIdNotProvidedError(false);
        if (values.password === undefined) setPasswordNotProvidedError(true); else setPasswordNotProvidedError(false);
        if (values.pseudo === undefined || values.password === undefined) return

        setIsCreateButtonClicked(true);
        userLogin(values.pseudo, values.password)
        .then(rq => rq.json())
        .then((rq) => {
            setIdNotProvidedError(false);
            setPasswordNotProvidedError(false);
            setPasswordOrIdIncorrect(false)
            dispatch(loginAction(rq.name, rq.token))
            createSession(rq.name, rq.token)
            history.push("/")
        }, () => {setIsCreateButtonClicked(false); setPasswordOrIdIncorrect(true)})
    }

    const form = 
                    <div className="connexion">
                        <section className = "sectionImgConnexion">
                            <img src={imgConnexion} alt="Connexion" className="imgConnexion"/>
                        </section>

                        <section className = "formConnexion">
                            <h2 className ="titleLogin">Hopopop faut se connecter !</h2>
                            <Form onSubmit={handleSumbit}>
                                {({handleSubmit}) => <form className="inFormConnexion" onSubmit={handleSubmit}>
                                        <article className="infoConnexion">
                                            <label className="labelConnexion" htmlFor="pseudo">Email</label>
                                            <Field className="inputConnexion is-invalid" name="pseudo" component="input" placeholder="RakanMainForever"></Field>
                                            {errorSpan(idNotProvidedError, "L'identifiant est obligatoire")}
                                        </article>
                                        <article className="infoConnexion">
                                            <label className="labelConnexion" htmlFor="Password" >Mot de passe</label>
                                            <Field className="inputConnexion is-invalid" name="password" component="input" type="password" placeholder="**********"></Field>
                                            {errorSpan(passwordNotProvidedError, "Le mot de passe est obligatoire")}
                                        </article >
                                        <article className="onButton">
                                            <button className="btnConnexion  custom-button is-invalid" type="submit" disabled={isCreateButtonClicked}>Connexion</button>
                                            {errorSpan(passwordOrIdIncorrect, "Identifiant ou mot de passe invalide !")}
                                            <Link className="goSignUp" to="/signUp">Creer un compte</Link>
                                        </article>
                                        
                                        
                                </form>}
                            </Form>

                            
                        </section>
                    </div>
                
                    
                   
    return form

}

export default SignIn;