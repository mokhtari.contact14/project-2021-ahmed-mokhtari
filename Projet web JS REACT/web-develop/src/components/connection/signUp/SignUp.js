import React, { useState } from 'react'
import { Form, Field } from "react-final-form"
import { Redirect, Link } from 'react-router-dom'
import { errorSpan, getClassForInputError } from '../common'
import "./SignUp.css"
import { userSignUp } from '../../../webservice/endpoints'

function SignUp(props) {
    const [passwordDoNotMatchError, setPasswordDoNotMatchError] = useState(null)
    const [pseudoNotProvidedError, setPseudoNotProvidedError] = useState(null)
    const [emailNotProvidedError, setEmailNotProvidedError] = useState(null)
    const [passwordNotProvidedError, setPasswordNotProvidedError] = useState(null)
    const [emailAlreadyInUseError, setEmailAlreadyInUseError] = useState(null)
    const [isUnknowError, setIsUnknowError] = useState(null)
    const [submittedSuccessfully, setSubmittedSuccessfully] = useState(false)
    const [redirect, setRedirect] = useState(false)
    const [isCreateButtonClicked, setIsCreateButtonClicked] = useState(false)
    
    // post redirection after account creation
    const waitForCountdown = (countdown) => {
        setTimeout(() => {
            if(countdown !== 0) {
                waitForCountdown(countdown-1);
            } else {
                setRedirect(true);
            }
        }, 1000);
    }

    if(redirect) {
        return <Redirect to="/signIn"></Redirect>
    }

    const TIME_BEFORE_REDIRECT = 3
    const formItems = {"pseudo": setPseudoNotProvidedError, "email": setEmailNotProvidedError, "password": setPasswordNotProvidedError, "confirm_password": setPasswordNotProvidedError}
    
    /**
     * send request to the server to create a new account
     * @param {Object} values 
     */
    const handleSubmit = (values) => {
        setIsCreateButtonClicked(true);
        for(const key of Object.keys(formItems)) {formItems[key](values[key] === undefined);}
        if(emailAlreadyInUseError) {setEmailAlreadyInUseError(false);}
        if(isUnknowError) {setIsUnknowError(false);}
        // Si tout les champs on été entrés
        if(Object.keys(values).length === 4) {
            if(values.confirm_password !== values.password) {
                setPasswordDoNotMatchError(true);
                setIsCreateButtonClicked(false);
                return;
            } else if (passwordDoNotMatchError) {setPasswordDoNotMatchError(false);}
            const rqBody = {"name": values["pseudo"], "email": values["email"], "password": values["password"]};
            // Do request
            userSignUp(rqBody.email, rqBody.name, rqBody.password)
            .then((rq) => {
                if (rq.status === 409) {
                    setEmailAlreadyInUseError(true);
                } else if (rq.status === 200){ 
                    setSubmittedSuccessfully(true);
                    waitForCountdown(TIME_BEFORE_REDIRECT-1)
                } else {
                    setIsUnknowError(true);
                }
                setIsCreateButtonClicked(false);
            })
        } else {
            setIsCreateButtonClicked(false);
        }
    }

    const customForm = 
                    <div className="container-fluid">
                        <div className="container text-center mt-4">
                            <div className="row">
                                <div className="col-lg-7 col-offset-6 centered rounded p-4 form_inscription">
                                <h2 className ="title_inscription text-light">Inscrivez-vous pour jouer</h2><hr />
                                <Form onSubmit={handleSubmit}>
                                    {({handleSubmit}) => <form className="form-group" onSubmit={handleSubmit}>
                                            <label className="labelpseudo" htmlFor="pseudo">Pseudo</label>
                                            <Field className={"form-control " + getClassForInputError(pseudoNotProvidedError)} name="pseudo" component="input" placeholder="RakanMainForever"></Field>
                                            {errorSpan(pseudoNotProvidedError, "Le pseudo est obligatoire")}
                                            <label className="labelemail" htmlFor="email">Email</label>
                                            <Field className={"form-control " + getClassForInputError(emailNotProvidedError, emailAlreadyInUseError)} name="email" component="input" placeholder="rakanforever@gmail.com"></Field>
                                            {errorSpan(emailNotProvidedError, "L'émail est obligatoire")}
                                            {errorSpan(emailAlreadyInUseError, "L'adresse email est déjà utilisée")}
                                            <label className="labelpassword" htmlFor="password">Mot de passe</label>
                                            <Field className={"form-control " + getClassForInputError(passwordDoNotMatchError, passwordNotProvidedError)} name="password" component="input" type="password" placeholder="**********"></Field>
                                            {errorSpan(passwordNotProvidedError, "Le mot de passe est obligatoire")}
                                            <label className="labelconfirm" htmlFor="confirm_password">Confirmer votre mot de passe</label>
                                            <Field className={"form-control " + getClassForInputError(passwordDoNotMatchError, passwordNotProvidedError)} name="confirm_password" component="input" type="password" placeholder="**********"></Field>
                                            {errorSpan(passwordDoNotMatchError, "Les mots de passe ne sont pas identiques ...")}
                                            <button className="custom-button is-invalid mt-4" type="submit" disabled={isCreateButtonClicked}>{isCreateButtonClicked ? "Chargement ..." : "Créer mon compte"}</button>
                                            {errorSpan(isUnknowError, "Une erreur inconnuue est survenue, merci de réessayer plus tard ...")}
                                    </form>}
                                </Form>
                                <Link className="goToSignIn" to="/signIn">J’ai déja un compte, je me connecte </Link>
                                </div>
                            </div>
                        </div>
                    </div>
    // display success message                
    const createdAccount = <div className="container text-center font-weight-bold align-center h4 mt-4">
            Votre compte a été créé, vous pouvez maintenant vous connecter.<br/> Vous allez être rediriger dans {TIME_BEFORE_REDIRECT} secondes vers la page de connexion
        </div>

    return submittedSuccessfully ? createdAccount : customForm
}




export default SignUp;
