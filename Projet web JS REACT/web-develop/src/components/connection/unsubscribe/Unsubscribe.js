import React, { useState } from 'react'
import { Form, Field } from "react-final-form"
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import { logoutAction } from '../../../redux/factories';
import { useHistory } from 'react-router-dom'
import { userUnsubscribe } from '../../../webservice/endpoints'
import { errorSpan } from '../common'
import './unsubscribe.css';

function Unsubscribe(props) {
    const [idNotProvidedError, setIdNotProvidedError] = useState(null)
    const [passwordOrIdIncorrect, setPasswordOrIdIncorrect] = useState(null)
    const [passwordNotProvidedError, setPasswordNotProvidedError] = useState(null)
    const [isDeletedAccount, setIsDeletedAccount] = useState(false)
    const history = useHistory();

    const dispatch = useDispatch();
    const userToken = useSelector(state => state.token)

    /**
     * send request to the server to delete account
     * @param {Object} values 
     */
    const handleSumbit = (values) => {
        if (values.pseudo === undefined) setIdNotProvidedError(true); else setIdNotProvidedError(false);
        if (values.password === undefined) setPasswordNotProvidedError(true); else setPasswordNotProvidedError(false);
        if (values.pseudo === undefined || values.password === undefined) return
        // Si tout les champs on été entrés
        setIsDeletedAccount(true)
        // Do request
        userUnsubscribe(values.pseudo, values.password, userToken)
        .then((rq) => {
            if(rq.status === 200) {
                setIdNotProvidedError(false);
                setPasswordNotProvidedError(false);
                setPasswordOrIdIncorrect(false);
                dispatch(logoutAction())
                history.push("/signIn")
            } else {
                setIsDeletedAccount(false); 
                setPasswordOrIdIncorrect(true)
            }
        });
    }

    const form = 
                <div className="confirmation__suppressionCompte">
                    <section className = "formSuppression">
                        <h2 className ="title">Confirmer votre décision !</h2> <hr />
                        <Form onSubmit={handleSumbit}>
                            {({handleSubmit}) => 
                                <form className="inFormSuppression" onSubmit={handleSubmit}>
                                    <article className="infoSuppression">
                                        <label className="labelSuppression" htmlFor="pseudo">Email</label>
                                        <Field className="inputSuppression is-invalid" name="pseudo" component="input" placeholder="RakanMainForever"></Field>
                                        {errorSpan(idNotProvidedError, "L'identifiant est obligatoire")}
                                    </article>
                                    <article className="infoSuppression">
                                        <label className="labelSuppression" htmlFor="Password" >Mot de passe</label>
                                        <Field className="inputSuppression is-invalid" name="password" component="input" type="password" placeholder="your password"></Field>
                                        {errorSpan(passwordNotProvidedError, "Le mot de passe est obligatoire")}
                                    </article>
                                    <button className="btn_confirm is-invalid" type="submit" disabled={isDeletedAccount}>Confirmer</button>
                                    {errorSpan(passwordOrIdIncorrect, "Identifiant ou mot de passe invalide !")}
                                </form>}
                        </Form>

                        <Link className="annulle_op" to="/">Je ne veux pas supprimer mon compte</Link>
                        </section>
                    </div>

    return form
}

export default Unsubscribe;