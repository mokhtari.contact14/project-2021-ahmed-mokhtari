export function getClassForInputError() {
    const values = Object.values(arguments)
    return values.every((arg) => arg == null) ? '' : (values.some((arg) => arg) ? 'is-invalid' : 'is-valid')
}

export function errorSpan(show, message) {
    return show ? <span className="invalid-feedback text-center">{message}</span> : ''
}