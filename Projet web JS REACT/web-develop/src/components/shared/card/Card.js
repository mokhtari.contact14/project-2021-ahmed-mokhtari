import React, { useEffect, useState } from 'react';
import "./Card.css";
import verso_card from './images/verso-card.png'

const getImgUrl = (champ_name) =>
  "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/" +
  champ_name +
  "_0.jpg";

function Card(props) {
    /* props : 
  champion = {
    id, name, title image, info, ...
  }
  isTurned: undefined (<=> false) | bool
  */

  /**
   * ? Is it necessary because based on timestamp
   * Perform a custom hash, by shuffling and transforming a string
   * @param {string} str The string to "hash"
   */
  const custom_hash = (str) => {
    const modifiedStr = str + str + "hash"
    return "c"+modifiedStr.split('').sort(function(){return 0.5-Math.random()}).join('')
  };

  // This will be the id of the card.
  // It is usefull to provide the drag feature, to select only the card
  // Based on current timestamp to provide an unique id
  const [hash] = useState(custom_hash(Date.now().toString()));

  return (
    props.isTurned ?
    <img id={hash} className="game-card" src={verso_card}/> :
      <div id={hash} className={"game-card " + props.cardClasses} onClick={props.onClickCallback}>
        <img src={getImgUrl(props.champion.key)} className="card-img" alt={props.champion.name}/>
        <div className="game-card-caption">{props.champion.name}</div>
        <div className="atq_pts">{props.champion.info.attack}</div>
        <div className="armor_pts">{props.champion.info.defense}</div>
      </div>
  );
}

export default Card;