import React from 'react'
import './Loader.css'

function Loader(props) {
    return <div className="loader-container">
        <div className="loader"></div>
        <div className="shadow"></div>
    </div>
}

export default Loader;