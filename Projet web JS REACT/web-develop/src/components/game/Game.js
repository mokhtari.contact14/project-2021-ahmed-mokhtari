import React, {useState, useEffect} from 'react'
import { useSelector } from 'react-redux'
import { matchEndTurn, matchGetCurrentGame, matchPickCard, matchPlayCard, matchAttack, matchAttackPlayer, cardsGetAll, matchEndMatch } from '../../webservice/endpoints'
import './Game.css'
import Card from '../shared/card/Card'
import user_pp from './images/p1.png'
import opponent_pp from './images/p2.png'
import Loader from '../shared/loader/Loader'
import { useHistory } from 'react-router-dom'

const Game = (props) => {
    const TIME_PER_TURN = 60 + 30 // 1 minute & 30 seconds
    const MAX_CHAMP_ON_BOARD = 5

    const [userPlayer, setUserPlayer] = useState(undefined);
    const [opponentPlayer, setOpponentPlayer] = useState(undefined);
    
    const [isMatchFinished, setIsMatchFinished] = useState(false);
    const [isMatchLoaded, setIsMatchLoaded] = useState(false)
    const [isUserTurn, setIsUserTurn] = useState(undefined)
    const [currentGame, setCurrentGame] = useState(undefined)
    const [selectedCard, setSelectedCard] = useState(null)
    const [timer, setTimer] = useState(TIME_PER_TURN)
    const [performingAction, setPerformingAction] = useState(false);

    const token = useSelector(state => state.token)
    const username = useSelector(state => state.username)
    const history = useHistory()
    
    /**
     * Check if a request is valid. If yes, perform a getmatch to refresh the view. Else, log the error in console
     * @param {RequestAnswer} requestAnswer The answer of the request
     */
    const requestCheck = (requestAnswer) => {
        if(requestAnswer.ok) getMatch()
        else {
            console.log(requestAnswer.text)
            setPerformingAction(false)
        }
    }

    /** Endpoint calls */

    /* Get match informations */
    const getMatch = () => {
        return matchGetCurrentGame(token)
        .then(rq => rq.json())
        .then(data => {setCurrentGame(data); setPerformingAction(false) ; 
            if(data && data.status.includes("won")) {
                setIsUserTurn(undefined) // No player turn
                setIsMatchFinished(true)
            };
            return data},
            (err) => {
                setIsUserTurn(undefined) // No player turn
                setIsMatchFinished(true)
            })
    }

    // Pick a card for the player
    const pickCard = () => {
        setPerformingAction(true)
        matchPickCard(token)
        .then(rq => {
            requestCheck(rq)
        })
    }

    // Play selected card for the player
    const playCard = (card) => {
        setPerformingAction(true)
        matchPlayCard(card, token)
        .then(rq => {
            requestCheck(rq)
        })
    }

    // End the turn for the player
    const endTurn = () => {
        setPerformingAction(true)
        matchEndTurn(token)
        .then(rq => {
            if(rq.ok) {
                setIsUserTurn(false)
                setSelectedCard(undefined)
                //* Le tour s'est bien passé.
            } else {
                // * Erreur (not your turn)
            }
            setPerformingAction(false)

        })
    }

    // Make a card attack another
    const attack = (playerCard, opponentCard) => {
        setPerformingAction(true)
        matchAttack(playerCard, opponentCard, token)
        .then(rq => requestCheck(rq))
    }

    // Make a card attack the opponent
    const attackPlayer = (playerCard) => {
        setPerformingAction(true)
        matchAttackPlayer(playerCard, token)
        .then(rq => requestCheck(rq))
    }

    /**
     * Set an interval that check opponent's player actions
     */
    const checkForOpponentTurnEnd = () => setInterval(() => {
        getMatch().then((data) => {
            if(data[userPlayer].turn) setIsUserTurn(true)
            else console.log(data)
        })
    }, 1000)

    /**
     * Set a timer for the player. At the end of the timer, end his turn
     * @param {int} time The amount of time of the timer, in seconds
     * Return the interval id
     */
    const setTimerInterval = (time) => {
        let timeV = time
        let timerId = setInterval(() => {
            getMatch()
            timeV = timeV - 1
            if(timeV === 0) {
                endTurn()
                setIsUserTurn(false)
                clearInterval(timerId)
            }
            setTimer(timeV)
        },1000);
        return timerId
    }

    /** 
     * UseEffect which determine who is player1 & 2 and who is starting
     * Only triggered once
    */
    useEffect(() => {
        /* When arriving on the page */
        // Check which player the user is (1 or 2)
        const waitForFirstTurn = () => {
            getMatch()
            .then((data) => {
                if(data.status.includes("pending")) {
                    setTimeout(waitForFirstTurn, 1000)
                    return
                }
                const a = data.player1.name === username ? "player1" : "player2"
                const b = a === "player1" ? "player2" : "player1"
                setUserPlayer(a)
                setOpponentPlayer(b)
                setIsUserTurn(data[a].turn)
                setIsMatchLoaded(true)
            })
        }
        waitForFirstTurn()
    }, [])

    /**
     * Trigerred at each set of user turn
     * Depending of the situation, it will set the player in waiting state (opponent turn) or current turn state & activate the timer
     */
    useEffect(() => {
        let timerId = 0;
        if(currentGame && !currentGame.status.includes("pending")) {
            if(isUserTurn == undefined) {} // If undefined, then, nothing happen
            else if(!isUserTurn) {
                timerId = checkForOpponentTurnEnd()
            } else {
                timerId = setTimerInterval(TIME_PER_TURN)
                pickCard()
            }
        }
        return () => {
            clearInterval(timerId)
        }

    }, [isUserTurn])

    /**
     * If match ended, then, end the match and go to home page after 10s
     */
    useEffect(() => {
        if(isMatchFinished) {
            matchEndMatch(token)
            .then(() => {
                // Redirection vers l'accueil 10 secondes après
                // si erreur -> match existe pas -> déjà supprimé -> on s'en fou
                setTimeout(
                    () => history.push("/"),
                    10000
                )
                
            })
        }
    }, [isMatchFinished])

    /** Methods */

    /* CLASSES GENERATOR */
    // get classes for the player card
    const getUserCardClasses = (card) => {
        const can_attack = !card.attack && isUserTurn ? "can-attack" : undefined
        const is_selected = selectedCard && selectedCard["key"] === card["key"] ? "is-selected" : undefined
        return performingAction ? "" : (is_selected ?? (can_attack ?? ""))
    }

    const getOpponentCardClasses = (card) => isUserTurn && selectedCard && !performingAction ? "can-be-attacked" : ""

    /* CLICK EVENTS */
    // When the user click on a card on his board
    const handleOnUserBoardClick = (card) => {
        if(performingAction) console.log("Action currently executed")
        else if(!isUserTurn) console.log("You can't attack while it isn't your turn")
        else {
            if(card.attack) console.log("The card has already attacked")
            else setSelectedCard(selectedCard === card ? undefined : card)
        }
    }
     //When the user clickon a card in his hand
    const handleOnUserHandClick = (card) => {
        if(performingAction) console.log("Action currently executed")
        else if(!isUserTurn) console.log("You can't attack while it isn't your turn")
        else {
            if(currentGame[userPlayer].board.length < MAX_CHAMP_ON_BOARD)
                playCard(card)
            else console.log("Board full, can't place it")
        }
    }

    // When player click on a card of the opponent board
    const handleOnOpponentBoardClick = (card) => {
        if(performingAction) console.log("Action currently executed")
        else if(!isUserTurn) console.log("You can't attack while it isn't your turn")
        else {
            if(selectedCard === undefined) console.log("You should select the card that will attack first")
            else {
                attack(selectedCard, card)
                /* This should be auto with the server */
                setSelectedCard(undefined)
            }
        }
    }

    // When player click on the opponent
    const handleOnOpponentClick = (card) => {
        if(performingAction) console.log("Action currently executed")
        else if(!isUserTurn) console.log("You can't attack while it isn't your turn")
        else {
            if(currentGame[opponentPlayer].board.length !== 0) console.log("The opponent board shall be empty")
            else if(selectedCard === undefined) console.log("You should select the card that will attack first")
            else {
                attackPlayer(selectedCard)
                setSelectedCard(undefined)
            }
        }
    }

    const quitGame = () => {
        if (window.confirm("Are you sure you want to quit?")) {
            matchEndMatch(token)
            .then(() => {
                setIsMatchFinished(true)
                setTimeout(
                    () => history.push("/"),
                    20000
                )
            })
        }
    }

    // Function that reproduce the usage of range() in python
    const range = (to) => {
        const arr = []
        for(let i = 0; i < to; i++) {
            arr.push(i)
        }
        return arr
    }

    // If the match isn't loaded, show the loader
    if(!isMatchLoaded) return <Loader/>
    if(isMatchFinished) return <div className="final-message">
        {currentGame.status.includes("won") ? ((currentGame.status.includes("1") ? currentGame.player1.name : currentGame.player2.name) + "a gagné la partie !") : "L'adversaire à quitté la partie ..."}
        <button className="otherButton" onClick={() => {history.push("/")}}>Retour au menu principal</button>
    </div>
    return <div className="game-window">
        <div className="opponent-board">
            <div className="user-informations">
                <img onClick={handleOnOpponentClick} src={opponent_pp} className={"user-pic " + (isUserTurn && currentGame[opponentPlayer].board.length === 0 ? "can-be-attacked" : "")} alt="Opponent"/>
                <div className="userName">{currentGame[opponentPlayer].name}</div>
            </div>
            <div className="opponent-cards">
                <div className="opponent-hand">
                    {range(currentGame[opponentPlayer].hand).map( (_,index) => <Card key={index} isTurned={true}/>)}
                </div>
                <div className="opponent-played-cards">{currentGame[opponentPlayer].board.map((card, index) => <Card key={index} onClickCallback={() => handleOnOpponentBoardClick(card)} cardClasses={getOpponentCardClasses(card)} champion={card}/>)}</div>
            </div>
        </div>
        <div className="separator">
            <button className="btn" onClick={quitGame}>Quitter le jeu</button>
            {isUserTurn ? <button class="otherButton"onClick={endTurn}>Fin de tour</button> : null}
            <div className="divider"></div>
            <div className="data-container">
                <div className="opponent-life">{currentGame[opponentPlayer].hp} ❤</div>
                <div className="timer-container">
                    {isUserTurn ? (Math.floor((timer/60)) + ":" + (timer%60 >= 10 ? timer%60 : "0"+ timer%60)) : "Adversaire"}
                </div>
                <div className="user-life">{currentGame[userPlayer].hp} ❤</div>
            </div>
        </div>
        <div className="user-board">
            <div className="user-informations">
                <img src={user_pp} className="user-pic" alt="User"/>
                <div className ="userName">{currentGame[userPlayer].name}</div>
                <div className ="deckUser">Deck : {currentGame[opponentPlayer].deck}</div>
            </div>
            <div className="user-cards">
                <div className="user-played-cards">{currentGame[userPlayer].board.map((card, index) => <Card key={index} onClickCallback={() => handleOnUserBoardClick(card)} cardClasses={getUserCardClasses(card)} champion={card}/>)}</div>
                <div className="user-hand">{currentGame[userPlayer].hand.map((card, index) => <Card key={index} cardClasses={isUserTurn && currentGame[userPlayer].board.length < MAX_CHAMP_ON_BOARD ? "can-be-placed": ""} onClickCallback={() => handleOnUserHandClick(card)} champion={card}/>)}</div>
            </div>
        </div>
    </div>
}

export default Game