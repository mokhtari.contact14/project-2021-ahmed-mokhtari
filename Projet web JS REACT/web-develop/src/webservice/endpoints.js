// #region UTILS

// The base URL of the application
const BASE_URL = "https://los.ling.fr" // * TO DEBUG. Original URL : https://los.ling.fr
// Build an URL depending of the path given, with, as base, the const URL above
const URL = (pPath) => BASE_URL + pPath

// HEADER for common request, that aren't involving an user
const BODY_JSON_HEADER = {"content-type": "application/json"}
// If an user is involved, pass his token, and build header
const TOKEN_HEADER = (token) => { return {"WWW-Authenticate": token}}

// Perform a PUT request
const put = (pUrl, pBody, pHeaders) => fetch(pUrl, { method: 'PUT', body: JSON.stringify(pBody), headers:pHeaders})
// Perform a POST request
const post = (pUrl, pBody, pHeaders) => fetch(pUrl, { method: 'POST', body: JSON.stringify(pBody), headers:pHeaders})
// Perform a GET request
const get = (pUrl, pToken) => fetch(pUrl, { headers: TOKEN_HEADER(pToken) })

// #endregion


// #region ENDPOINTS

// #region MATCHMAKING

/**
 * Set in queue the current logged user
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchmkGoInQueue = (pToken) => get(URL("/matchmaking/participate"), pToken)
/**
 * Remove the logged player from the current queue
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchmkGoOutOfQueue = (pToken) => get(URL("/matchmaking/unparticipate"), pToken)
/**
 * Get all available matchmaking available on the server
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchmkGetAllAvailable = (pToken) => get(URL("/matchmaking/getAll"), pToken)
/**
 * Make the logged player request a match with an open matchmaking
 * @param {?} pMatchmakingId The matchmakingId the user want to participate
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchmkRequest = (pMatchmakingId, pToken) => get(URL(`/matchmaking/request?matchmakingId=${pMatchmakingId}`), pToken) // ? valid ?
/**
 * Make the user accept a request from an other player
 * @param {?} pMatchmakingId The matchmakingId the user want to participate
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchmkAcceptRequest = (pMatchmakingId, pToken) => get(URL(`/matchmaking/acceptRequest?matchmakingId=${pMatchmakingId}`), pToken) // ? valid ?

// #endregion

// #region MATCHS
/**
 * IMPORTANT
 * Those endpoints are only available when the user is logged, and IN A MATCH 
 * (the only one available at every moment is matchGetAll)
 */

/**
 * Get all match which are actually on the server
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchGetAll = (pToken) => get(URL("/match/getAllMatch"), pToken)

/**
 * Get the match of the current logged user
 * ! This endpoint has to be called at regular interval, to have an update of the board, when an action have been done
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchGetCurrentGame = (pToken) => get(URL("/match/getMatch"), pToken)

/**
 * Init the deck of the player which entered in a match
 * @param {Array[card]} pChoosedDeck The deck that choosed the user
 * @param {string} pToken The token gived at the connexion of the user
 * 
 * @Precondition choosedDeck.length == 20 && no duplicated cards
 */
export const matchInitDeck = (pChoosedDeck, pToken) =>  {
    const deck = pChoosedDeck.map(card => { return {key: card.key}})
    ;
    return get(URL(`/match/initDeck?deck=${JSON.stringify(deck)}`), pToken) // ! to test
}

/**
 * Pick a card for the in game player
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchPickCard = (pToken) => get(URL("/match/pickCard"), pToken)

/**
 * Play a card for the player
 * @param {card} pCard The card to play
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchPlayCard = (pCard, pToken) => get(URL(`/match/playCard?card=${pCard.key}`), pToken)

/**
 * Make a card attack an other
 * @param {card} pAttackingCard The card who attack
 * @param {card} pAttackedCard The card which have been attacked
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchAttack = (pAttackingCard, pAttackedCard, pToken) => get(URL(`/match/attack?card=${pAttackingCard.key}&ennemyCard=${pAttackedCard.key}`), pToken)

/**
 * Make a card attack the opponent
 * @param {card} pAttackingCard The card which is attacking
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchAttackPlayer = (pAttackingCard, pToken) => get(URL(`/match/attackPlayer?card=${pAttackingCard.key}`), pToken)

/**
 * End the turn of the player
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchEndTurn = (pToken) => get(URL("/match/endTurn"), pToken)

/**
 * End the match
 * @param {string} pToken The token gived at the connexion of the user
 */
export const matchEndMatch = (pToken) => get(URL("/match/finishMatch"), pToken)

// #endregion

// #region CARDS

/**
 * Get all cards available on the server
 * @param {string} pToken The token gived at the connexion of the user
 */
export const cardsGetAll = (pToken) => get(URL("/cards"), pToken)

// #endregion

// #region USERS

/**
 * Get all users of the server
 * @param {string} pToken The token gived at the connexion of the user
 */
export const userGetAll = (pToken) => get(URL("/users/getAll"), pToken)

/**
 * Sign up a user, and create an account
 * @param {string} pEmail The email of the user
 * @param {string} pName The name of the user
 * @param {string} pPassword The password of the user
 * 
 */
export const userSignUp = (pEmail, pName, pPassword) => put(URL("/user"), {"name": pName, "email": pEmail, "password": pPassword}, {...BODY_JSON_HEADER})

/**
 * Unsubscribe an user from the application
 * @param {string} pEmail The email of the user
 * @param {string} pPassword The password of the user
 * @param {string} pToken The token gived at the connexion of the user
 */
export const userUnsubscribe = (pEmail, pPassword, pToken) => get(URL(`/users/unsubscribe?email=${pEmail}&password=${pPassword}`), pToken)

/**
 * Log an user in
 * @param {string} pEmail The email of the user
 * @param {string} pPassword The password of the user
 */
export const userLogin = (pEmail, pPassword) => post(URL("/login"), {"email": pEmail, "password": pPassword}, {...BODY_JSON_HEADER})

/**
 * Log an user out
 * @param {string} pToken The token gived at the connexion of the user
 */
export const userLogout = (pToken) => post(URL("/logout"), {}, {...TOKEN_HEADER(pToken), ...BODY_JSON_HEADER})

/**
 * Check if the current player is logged in
 * @param {string} pToken The token gived at the connexion of the user
 */
export const userCheckConnexion = (pToken) => get(URL("/users/amIConnected"), pToken)

// #endregion

// #endregion
