import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import reportWebVitals from './reportWebVitals';
import NavBar from './components/common/navbar/NavBar';
import SignIn from './components/connection/signIn/SignIn';
import SignUp from './components/connection/signUp/SignUp';
import Unsubscribe from './components/connection/unsubscribe/Unsubscribe';
import Deck from './components/menu/deck/Deck';
import Matchmaking from './components/menu/matchmaking/Matchmaking';
import HomePage from './components/menu/homePage/HomePage';
import HomeLogged from './components/menu/homeLogged/HomeLogged';
import SeeMore from './components/common/docs/SeeMore/SeeMore';
import Contact from './components/common/docs/Contact/Contact';
import Footer from './components/common/footer/Footer';
import { createStore } from 'redux';
import { connexionReducer } from './redux/reducer'
import { Provider, useSelector } from 'react-redux'
import './index.css'
import Game from './components/game/Game';

/**
 * function to perform a conversion of a string to an array object 
 * because we can only store strings in a session storage
 */
const convert_string_to_array = () => {
  var value = sessionStorage.getItem('lastSavedDeck');
  var res = value ? JSON.parse(value) : [];
  return res;
}

/**
 * object used to initialize the redux store from a session storage so that when the user refresh the page remain logged in
 */
const initialState = {
  username: sessionStorage.getItem('name'),
  token: sessionStorage.getItem('token'),
  isLogged: sessionStorage.getItem('token') ? true : false,
  lastSavedDeck: convert_string_to_array()
}

/**
 * Store creation
 */
const store = createStore(connexionReducer, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

/**
 * Page routing 
 */
const Routing = () => {
  const isLogged = useSelector(state => state.isLogged)
  const notLoggedSwitch = <Switch>
                        <Route path="/" exact component={HomePage}></Route>
                        <Route path="/signUp"  component={SignUp}></Route>
                        <Route path="/signIn" component={SignIn}></Route>
                      </Switch>
  const loggedSwitch = <Switch>
                            <Route path="/" exact component={HomeLogged}></Route>
                            <Route path="/createDeck" component={Deck}></Route>
                            <Route path="/matchmaking" component={Matchmaking}></Route>
                            
                            <Route path="/unsubscribe" component={Unsubscribe}></Route>
                            <Route path="/game" component={Game}></Route>
                          </Switch>
  return (
    <Router>
      <div className="main-container">
        <NavBar/>
        {isLogged ? loggedSwitch : notLoggedSwitch}
        <Route path="/seeMore" component={SeeMore}></Route>
        <Route path="/contact" component={Contact}></Route>
      </div>
      <Footer/>
    </Router>
  )
}

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode thirdParty={false}>
      <Routing/>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
