package Timmo.BienImmobilier;

import java.io.Serializable;

public class Terrain extends BienImmobilier  implements Serializable {
    private static final long serialVersionUID= 1023L;

    private float longueurFacade;
    private float surfaceSol;

    public Terrain(float prix, String adresse,String dateDispo,Orientation orientation, float surfaceSol, float longueurFacade) {
        super(prix, adresse ,dateDispo,orientation);
        this.surfaceSol = surfaceSol;
        this.longueurFacade = longueurFacade;
    }
    public float getSurfaceSol() {
        return surfaceSol;
    }
    public void setSurfaceSol(float surface) {
        this.surfaceSol = surface;
    }

    public float getlongueurFacade() {
        return longueurFacade;
    }

    public void setLongueurFacade(float longueur) {
        this.longueurFacade = longueurFacade;
    }

    public String toString() {
        return " Terrain  "+super.toString()+",  la surface au sol: "+this.getSurfaceSol()+
                ",  la longueur de façade: "+this.getlongueurFacade();
    }
    }

