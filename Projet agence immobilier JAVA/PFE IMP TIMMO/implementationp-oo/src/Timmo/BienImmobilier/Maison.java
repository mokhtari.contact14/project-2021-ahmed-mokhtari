package Timmo.BienImmobilier;

import java.io.Serializable;

public class Maison extends BienImmobilier implements Serializable {
    private static final long serialVersionUID= 1022L;

    private float surfaceHbitable;
    private int nbreDePiece;
    private int nbreEtage;
    private String moyenDeChauffage;


    public Maison( float prix, String adresse, Orientation orientation, String dateDispo, float surface, int nbPieces, int nbEtages, String chauffage) {
        super(prix, adresse, dateDispo, orientation);
        this.surfaceHbitable = surface;
        this.nbreDePiece = nbPieces;
        this.nbreEtage = nbEtages;
        this.moyenDeChauffage = chauffage;
    }
    public float getSurfaceHbitable() {
        return surfaceHbitable;
    }

    public void setSurfaceHbitable(float surface) {
        this.surfaceHbitable = surface;
    }

    public int getNbreDePiece() {
        return nbreDePiece;
    }

    public void setNbreEtage(int nbPieces) {
        this.nbreEtage = nbPieces;
    }

    public int getNbEtage() {
        return nbreEtage;
    }

    public void setNbEtage(int nbEtages) {
        this.nbreEtage = nbEtages;
    }

    public String getMoyenDeChauffage() {
        return moyenDeChauffage;
    }

    public void setChauffage(String chauffage) {
        this.moyenDeChauffage = moyenDeChauffage;
    }

    public String toString() {
        return " Maison : "+super.toString()+", surface du maison est : "+this.getSurfaceHbitable()+
                ",  nombre des pièces est : "+this.getNbreDePiece()+
                ",  nombre d’étages est : "+this.getNbEtage()+
                ",  moyen de chauffage est : "+this.getMoyenDeChauffage();
    }


}