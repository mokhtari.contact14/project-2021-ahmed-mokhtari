package Timmo.BienImmobilier;
import Timmo.Agence.Commission;
import Timmo.Agence.MandatDeVente;
import Timmo.Agence.RDV;
import Timmo.Client.Client;
import Timmo.Publicité.Publicite;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class BienImmobilier implements Serializable  {
    private static final long serialVersionUID= 1020L;

    private ArrayList<Client> client;
    private ArrayList<Client> visiteur;
    private ArrayList<RDV> rdv;
    private float prix;
    private String dateVente;
    private String adresse;
    public enum Orientation{Nord, Est, Sud, Ouest};
    private Orientation orientation;
    private MandatDeVente mandat;
    private String dateDispo;
    private boolean Vendre;
    private Commission commission;

    public BienImmobilier(float prix, String adresse, String dateDispo,Orientation orientation) {
        this.prix = prix;
        this.adresse = adresse;
        this.dateDispo = dateDispo;
        this.dateVente =  null;
        this.mandat = null;
        this.Vendre= false;
        this.orientation= orientation;
        this.client= new ArrayList<Client>();
        this.visiteur= new ArrayList<>();
        this.rdv= new ArrayList<>();
        this.commission= new Commission(prix,10);
    }

    public ArrayList<Client> getClient() {
        return client;
    }

    public void setClient(ArrayList<Client> client) {
        this.client = client;
    }

    public ArrayList<RDV> getRdv() {
        return rdv;
    }

    public void setRdv(ArrayList<RDV> rdv) {
        this.rdv = rdv;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getDateVente() {
        return dateVente;
    }

    public void setDateVente(String dateVente) {
        this.dateVente = dateVente;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public MandatDeVente getMandat() {
        return mandat;
    }

    public void setMandat(MandatDeVente mandat) {
        this.mandat = mandat;
    }

    public String getDateDispo() {
        return dateDispo;
    }

    public void setDateDispo(String dateDispo) {
        this.dateDispo = dateDispo;
    }

    public boolean okVendre() {
        return Vendre;
    }

    public void setVendre() {
        this.Vendre = true;
    }

    public Commission getCommission() {
        return commission;
    }

    public void setCommission(Commission commission) {
        this.commission = commission;
    }
    public void ajoutClient(Client client) {
        this.client.add(client);
    }
    public void ajoutRdv(RDV rrdv) {
        this.rdv.add(rrdv);
    }

    @Override
    public String toString() {
        return
                " prix  : " + prix +
                ", adresse  : '" + adresse + '\'' +
                ", orientation=" + orientation +
                ", la date de Disponibiliter est : '" + dateDispo + '\'' +
                '}';
    }
}









