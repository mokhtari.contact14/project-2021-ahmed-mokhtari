package Timmo.BienImmobilier;

import java.io.Serializable;

public class Appartement extends BienImmobilier implements Serializable {
    private static final long serialVersionUID= 1021L;


    private int NbreDePiece;
    private int NumeroEtage;
    private float ChargeMensuelle;

    public Appartement(float prix, String adresse,String dateDispo,Orientation orientation, int nbreDePiece, int numeroEtage, float chargeMensuelle) {
        super(prix, adresse,dateDispo,orientation);
        this.NumeroEtage = numeroEtage;
        this.NbreDePiece = nbreDePiece;
        this.ChargeMensuelle = chargeMensuelle;
    }

    public int getNbreDePiece() {
        return NbreDePiece;
    }

    public void setNbreDePiece(int nbreDePiece) {
        this.NbreDePiece = nbreDePiece;
    }

    public int getNumeroEtage() {
        return NumeroEtage;
    }

    public void setNumeroEtage(int numeroEtage) {
        this.NumeroEtage = numeroEtage;
    }

    public float getChargeMensuelle() {
        return ChargeMensuelle;
    }

    public void setChargeMensuelle(float chargeMensuelle) {
        this.ChargeMensuelle = chargeMensuelle;
    }

    @Override
    public String toString() {
        return " Appartement : " + super.toString()+
                "  nombre De Piece est : " + NbreDePiece +
                ", numero d'etage est : " + NumeroEtage +
                ", charge Mensuelle est : " + ChargeMensuelle;
    }
}


