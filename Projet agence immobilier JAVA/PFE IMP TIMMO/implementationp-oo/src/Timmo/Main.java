package Timmo;
import Timmo.Agence.Agence;
import Timmo.BienImmobilier.Appartement;
import Timmo.BienImmobilier.BienImmobilier;
import Timmo.BienImmobilier.Maison;
import Timmo.BienImmobilier.Terrain;
import Timmo.Client.Client;
import Timmo.Agence.RDV;
import Timmo.Client.PersonneMorale;
import Timmo.Client.PersonnePhysique;
import Timmo.Client.Voeux;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {
    public static void listAgence() {
        System.out.print("#####################---- L'agence immobiliére Timmo ----######################### \n");
        System.out.print("#                                                                                      #\n");
        System.out.print("#  1.  Ajouter un  client                      2.  Afficher la liste des clients       # \n");
        System.out.print("#  3.  Ajouter un  bien                        4. Afficher la liste de bien            # \n");
        System.out.print("#  5.  Prendre un rendez-vous                  6. Afficher la liste des rendez-vous    # \n");
        System.out.print("#  7.  Ajouter promesse                        8. Afficher la liste de promesse        # \n ");
        System.out.print("# 9.  Ajouter un mandat                       10. Afficher la liste des mandats       # \n");
        System.out.print("#  11. Ajouter un publicité                    12. Afficher la liste des publicité     # \n");
        System.out.print("#  13. Afficher la liste des voeux             14. Afficher les statistiques           # \n");
        System.out.print("#                                  0. Exit                                             #\n");
        System.out.print("#####################---- L'agence immobiliére Timmo ----######################### \n");
    }
    public static int menu() {
        Scanner clavier = new Scanner(System.in);
        int cpt = -1;
        System.out.print("1. Retour à menu principal. \t\t\t\t 0. Exit \n");
        while (cpt != 0 && cpt != 1) {
            try {
                System.out.print(" Votre choix  : ");
                cpt = clavier.nextInt();
            } catch(InputMismatchException e) {
                clavier.next();
            }
        }
        System.out.print("\n");
        return cpt;
    }
    public static void main(String[] args) {

        Agence serial = Agence.deserialisation();
        Scanner clavier = new Scanner(System.in);
        int choix = -1, retour =0;
        while(true) {
            switch(choix) {
                case -1:
                    listAgence();
                    while (choix < 0 || choix > 14) {
                        try {
                            System.out.print(" Le numéro d'opération : ");
                            choix = clavier.nextInt();
                        } catch (InputMismatchException e) {
                            clavier.next();
                        }
                    }
                    System.out.print("\n");
                    break;
                case 0:
                    serial.serialisation();
                    System.exit(1);
                case 1://Ajouter un nouveau client
                    Agence.choixClient tc = null;
                    int choixNumuro =0 ,numeroStatutClient =0 ,sirenClient = 0, numeroVoeux = 0;
                    String adresse = null, nmTel = null, email = null, nom=null,prenom = null, formeJuridique = null, adresseVoeux = null;
                    Client.Statut statutClient=null;
                    Voeux.TypeDeBien typeVoeuxClient = null;
                    float prixClient = 0;
                    System.out.println(" Type du client ? :");
                    System.out.println("1. Physique \t\t\t\t 2. Morale ");
                    while (choixNumuro != 1  &&  choixNumuro != 2) {
                        try {
                            System.out.print(" choisir le type du client  : ");
                            choixNumuro = clavier.nextInt();
                        } catch(InputMismatchException e) {
                            clavier.next();
                        }
                    }
                    if (choixNumuro ==1){ //Personne Physique
                    tc = Agence.choixClient.Physique;
                        Scanner txt = new Scanner(System.in);
                        while (adresse==null && nmTel==null && email==null && prenom==null && nom==null) {
                            System.out.print("Nom du client : ");
                            nom = txt.nextLine();
                            System.out.print("Prénom du client : ");
                            prenom = txt.nextLine();
                            System.out.print("Adresse du client : ");
                            adresse = txt.nextLine();
                            System.out.print("Numéro de téléphone du client : ");
                            nmTel = txt.nextLine();
                            System.out.print("Email du client : ");
                            email = txt.nextLine();
                            System.out.println("Votre statut  : ");
                            System.out.println("1. Acheteur \t\t\t 2. Vendeur \t\t\t 3. Notaire ");
                            while (numeroStatutClient != 1 && numeroStatutClient != 2 && numeroStatutClient!=3) {
                                try {
                                    System.out.print("Choisir le bien : ");
                                    numeroStatutClient = txt.nextInt();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                            }
                            if(numeroStatutClient==1) statutClient=Client.Statut.Acheteur;
                            if(numeroStatutClient==2) statutClient=Client.Statut.Vendeur;
                            if(numeroStatutClient==3) statutClient=Client.Statut.Notaire;
                            if(statutClient==Client.Statut.Acheteur) {
                                System.out.println("Type du bien  : ");
                                System.out.println("1. Appartement \t\t\t 2. Maison \t\t\t 3. Terrain ");
                                while (numeroVoeux != 1 && numeroVoeux != 2 && numeroVoeux!=3) {
                                    try {
                                        System.out.print(" Votre choix : ");
                                        numeroVoeux = txt.nextInt();
                                    } catch(InputMismatchException e) {
                                        txt.next();
                                    }
                                }
                                if(numeroVoeux==1) typeVoeuxClient=Voeux.TypeDeBien.Appartement;
                                if(numeroVoeux==2) typeVoeuxClient=Voeux.TypeDeBien.Maison;
                                if(numeroVoeux==3) typeVoeuxClient=Voeux.TypeDeBien.Terrain;
                                try {
                                    System.out.print("Prix du bien : ");prixClient = txt.nextFloat();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                                while(adresseVoeux==null) {
                                    System.out.print("L'adresse du bien  : ");
                                    adresseVoeux = txt.nextLine();
                                }
                            }
                        }
                    }
                    if(choixNumuro ==2) { //Personne morale
                        tc = Agence.choixClient.Morale;
                        Scanner txt = new Scanner(System.in);
                        while (adresse==null && nmTel==null && email==null && formeJuridique==null && nom==null && sirenClient==0) {
                            System.out.print("Nom du client : "); nom = txt.nextLine();
                            System.out.print("Adresse du client : "); adresse = txt.nextLine();
                            System.out.print("Téléphone du client : "); nmTel = txt.nextLine();
                            System.out.print("Email du client : "); email = txt.nextLine();
                            System.out.print("Forme juridique du client : "); formeJuridique = txt.nextLine();
                            System.out.print("Siren du client : "); sirenClient = txt.nextInt();
                            System.out.println("\n Votre statut :");
                            System.out.println("1. Acheteur \t\t 2. Vendeur \t\t 3. Notaire ");
                            while (numeroStatutClient != 1 && numeroStatutClient != 2 && numeroStatutClient!=3) {
                                try {
                                    System.out.print("choisir le type statut  : ");
                                    numeroStatutClient = txt.nextInt();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                            }
                            if(numeroStatutClient==1) statutClient=Client.Statut.Acheteur;
                            if(numeroStatutClient==2) statutClient=Client.Statut.Vendeur;
                            if(numeroStatutClient==3) statutClient=Client.Statut.Notaire;
                            if(statutClient==Client.Statut.Acheteur) {
                                System.out.println("Type du bien :");
                                System.out.println("1. Appartement \t\t\t 2. Maison \t\t\t 3. Terrain ");
                                while (numeroVoeux != 1 && numeroVoeux != 2 && numeroVoeux!=3) {
                                    try {
                                        System.out.print("Essayer de choisir le bien : ");
                                        numeroVoeux = txt.nextInt();
                                    } catch(InputMismatchException e) {
                                        txt.next();
                                    }
                                }
                                if(numeroVoeux==1) typeVoeuxClient=Voeux.TypeDeBien.Appartement;
                                if(numeroVoeux==2) typeVoeuxClient=Voeux.TypeDeBien.Maison;
                                if(numeroVoeux==3) typeVoeuxClient=Voeux.TypeDeBien.Terrain;
                                try {
                                    System.out.print("Prix du bien  : ");prixClient = txt.nextFloat();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                                while(adresseVoeux==null) {
                                    Scanner cc = new Scanner(System.in);
                                    System.out.print("Adresse du bien  : ");adresseVoeux = cc.nextLine();
                                }
                            }
                        }
                    }
                    if (choixNumuro ==1){
                        PersonneMorale pm = new PersonneMorale(adresse, nmTel,email, statutClient, nom,formeJuridique, sirenClient);
                            Voeux vo = new Voeux(typeVoeuxClient ,prixClient, adresseVoeux);serial.addClient(pm,vo);
                    }if (choixNumuro ==2) {
                    PersonnePhysique pp = new PersonnePhysique(adresse, nmTel, email, statutClient, nom, prenom);
                        Voeux vo = new Voeux(typeVoeuxClient, prixClient, adresseVoeux);serial.addClient(pp, vo);
                }
                    System.out.println("Client est enregistrer");
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;
                case 2:
                    System.out.println("Afficher la liste des clients : ");
                    System.out.print(serial.afficherClt());
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 3://Ajouter un  bien
                    Agence.leBien lbien =null; BienImmobilier.Orientation orientation = null;
                    int nmBien = 0, nmOrientation=0, nbPiece =0, nbEtage =0;
                    String dateDisponibiliter = null, adresseBien = null, chauffage =null;
                    float prix =0, ChargeMensuelle =0, surface =0, longueur =0;
                    System.out.println("Menu du bien ? : ");
                    System.out.println("1. Appartement \t\t\t 2. Maison \t\t\t 3. Terrain ");
                    while (nmBien != 1 && nmBien != 2 && nmBien != 3) {
                        try {
                            System.out.print(" choisir le bien : ");
                            nmBien = clavier.nextInt();
                        } catch(InputMismatchException e) {
                            clavier.next();
                        }
                    }
                    if(nmBien==1) {//Appartement
                        lbien = Agence.leBien.Appartement;
                        Scanner txt = new Scanner(System.in);
                        while (adresseBien==null && dateDisponibiliter==null && nbEtage==0 && nbPiece==0  && ChargeMensuelle==0) {
                            System.out.print("L'adresse du bien : ");
                            adresseBien = txt.nextLine();
                            System.out.print("La date de disponibilité : ");
                            dateDisponibiliter = txt.nextLine();
                            System.out.print("Prix du bien : ");
                            prix = txt.nextFloat();
                            System.out.print("Nombre d'étages : ");
                            nbEtage = txt.nextInt();
                            System.out.print("Nombre de pieces  : ");
                            nbPiece = txt.nextInt();
                            System.out.print("Charge mensuel d'appartement : ");
                            ChargeMensuelle = txt.nextFloat();
                            System.out.println("L'orientation du bien : ");
                            System.out.println("1. Nord \t\t 2. Est \t\t 3. Sud \t\t 4. Ouest ");
                            while (nmOrientation != 1 && nmOrientation != 2 && nmOrientation!=3 && nmOrientation!=4) {
                                try {
                                    System.out.print(" choisir l'orientation : ");
                                    nmOrientation = txt.nextInt();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                            }
                            if(nmOrientation==1) orientation=BienImmobilier.Orientation.Nord;
                            if(nmOrientation==2) orientation=BienImmobilier.Orientation.Est;
                            if(nmOrientation==3) orientation=BienImmobilier.Orientation.Sud;
                            if(nmOrientation==4) orientation=BienImmobilier.Orientation.Ouest;
                        }
                        Appartement a = new Appartement(prix, adresseBien, dateDisponibiliter , orientation, nbPiece,  nbEtage, ChargeMensuelle);
                        serial.addBien(a);
                    }
                    if(nmBien==2) {//Maison
                        lbien = Agence.leBien.Maison;
                        Scanner txt = new Scanner(System.in);
                        while (adresseBien==null && dateDisponibiliter==null && nbEtage==0 && nbPiece==0 &&  chauffage==null && surface==0 ) {
                            System.out.print("L'adresse du bien : ");
                            adresseBien = txt.nextLine();
                            System.out.print("La date de disponibilité : ");
                            dateDisponibiliter = txt.nextLine();
                            System.out.print("Prix du bien : ");
                            prix = txt.nextFloat();
                            System.out.print("Nombre d'étages : ");
                            nbEtage = txt.nextInt();
                            Scanner tt = new Scanner(System.in);
                            System.out.print("Chauffage : ");
                            chauffage = tt.nextLine();
                            System.out.print("Nombre de pieces  : ");
                            nbPiece = txt.nextInt();
                            System.out.print(" La surface du bien : ");
                            surface= txt.nextFloat();
                            System.out.print(" \n 1. Nord \t\t 2. Est \t\t 3. Sud \t\t 4. Ouest \n");
                            while (nmOrientation != 1 && nmOrientation != 2 && nmOrientation!=3 && nmOrientation!=4) {
                                try {
                                    System.out.print(" Choisir l'orientation : ");
                                    nmOrientation = txt.nextInt();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                            }
                            if(nmOrientation==1) orientation=BienImmobilier.Orientation.Nord;
                            if(nmOrientation==2) orientation=BienImmobilier.Orientation.Est;
                            if(nmOrientation==3) orientation=BienImmobilier.Orientation.Sud;
                            if(nmOrientation==4) orientation=BienImmobilier.Orientation.Ouest;
                        }
                        Maison m = new Maison( prix, adresseBien, orientation, dateDisponibiliter, surface, nbPiece,nbEtage,chauffage);
                        serial.addBien(m);
                    }
                    if(nmBien==3) {//Terrain
                        lbien = Agence.leBien.Terrain;
                        Scanner txt = new Scanner(System.in);
                        while (adresseBien==null && dateDisponibiliter==null && longueur==0 && surface==0) {
                            System.out.print("L'adresse du bien : ");
                            adresseBien = txt.nextLine();
                            System.out.print("La date de disponibilité : ");
                            dateDisponibiliter = txt.nextLine();
                            System.out.print("Prix du bien : ");
                            prix = txt.nextFloat();
                            System.out.print("La surface du bien : ");
                            surface= txt.nextFloat();
                            System.out.print("Longeur du bien : ");
                            longueur= txt.nextFloat();
                            System.out.print("L'orientation du bien :\n");
                            System.out.print("1. Nord \t\t 2. Sud \t\t 3. Est \t\t 4. Ouest \n");
                            while (nmOrientation != 1 && nmOrientation != 2 && nmOrientation!=3 && nmOrientation!=4) {
                                try {
                                    System.out.print("Choisir l'orientation : ");
                                    nmOrientation = txt.nextInt();
                                } catch(InputMismatchException e) {
                                    txt.next();
                                }
                            }
                            if(nmOrientation==1) orientation=BienImmobilier.Orientation.Nord;
                            if(nmOrientation==2) orientation=BienImmobilier.Orientation.Sud;
                            if(nmOrientation==3) orientation=BienImmobilier.Orientation.Est;
                            if(nmOrientation==4) orientation=BienImmobilier.Orientation.Ouest;
                        }
                        Terrain t = new Terrain( prix, adresseBien, dateDisponibiliter,orientation, surface, longueur);
                        serial.addBien(t);
                    }
                    System.out.println(" Le bien est enregistrer");
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 4:
                    System.out.print("Afficher la liste des biens :\n");
                    System.out.print(serial.afficherBiens());
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 5://demander un rdv
                    String dateRdv = null, emailRdv = null, adresseRdv = null; Client clientRdv = null;RDV.RdvPour Rdvpour = null; BienImmobilier bienRdv = null; int numChoix = 0;
                    System.out.print("Prendre un rendez-vous :\n\n");
                    Scanner txt = new Scanner(System.in);
                    while(dateRdv == null && emailRdv==null && adresseRdv==null) {
                        System.out.print(" La Date du rdv : ");
                        dateRdv = txt.nextLine();
                        System.out.print("Email du client : ");
                        emailRdv = txt.nextLine();
                        clientRdv = serial.getClient(emailRdv);
                        System.out.print("Adresse du bien : ");
                        adresseRdv = txt.nextLine();
                        bienRdv =serial.getBien(adresseRdv);
                    }
                    if(bienRdv!=null && clientRdv!=null) {
                        System.out.println("le rdv pour : ");
                        System.out.print("1. Visite \t\t 2. Vente \t\t 3. Mandat \n");
                        while (numChoix != 1 && numChoix != 2 && numChoix != 3) {
                            try {
                                System.out.print("Tapez le numéro du choix : ")
                                ;numChoix = clavier.nextInt();
                            } catch (InputMismatchException e) {
                                clavier.next();
                            }
                        }
                        if (numChoix == 1) {
                            Rdvpour = RDV.RdvPour.Visite;
                                System.out.println("Le rdv est enregistrer");
                            RDV Rmandat = new RDV(dateRdv, clientRdv, Rdvpour, bienRdv);
                            serial.addRdv(Rmandat);
                        }
                        if (numChoix == 2) {
                            Rdvpour = RDV.RdvPour.Vente;
                            System.out.println("Le rdv est enregistrer");
                            RDV Rvente = new RDV(dateRdv, clientRdv, Rdvpour, bienRdv);
                            serial.addRdv(Rvente);
                            }

                        if (numChoix == 3) {
                            Rdvpour = RDV.RdvPour.Mandat;
                            System.out.println("Le rdv est enregistrer");
                            RDV Rmandat = new RDV(dateRdv, clientRdv, Rdvpour, bienRdv);
                            serial.addRdv(Rmandat);
                        }

                    } else {
                            System.out.print("les informations incorrectes  \n");
                    }
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 6:
                    System.out.println("Afficher  la liste des rendez-vous :");
                    System.out.print(serial.afficherRDV());
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 7://ajouter promesse de vente
                    float valeur = 0;
                    BienImmobilier promesse = null;
                    String promesseAdresse = null, emailAcheteur = null,emailVendeur = null, emailNotaire = null;
                    Client promesseAcheteur = null, promesseVendeur = null, promesseNotaire = null;

                    System.out.println("La promesse de vente :");
                    while (valeur<=0 ) {
                        try {
                            System.out.print("la valeur de promesse ?: ");
                            valeur = clavier.nextFloat();
                        } catch(InputMismatchException e) {
                            clavier.next();
                        }
                    }
                    Scanner tt = new Scanner(System.in);
                    while(promesseAdresse == null && emailAcheteur==null && emailVendeur==null && emailNotaire==null) {
                        System.out.print("L'adresse du bien : ")
                        ;promesseAdresse = tt.nextLine();
                        promesse = serial.getBien(promesseAdresse);
                        System.out.print("Email du acheteur : ")
                        ;emailAcheteur = tt.nextLine();
                        promesseAcheteur = serial.getClient(emailAcheteur);
                        System.out.print("Email du vendeur : ")
                        ;emailVendeur = tt.nextLine();
                        promesseVendeur = serial.getClient(emailVendeur);
                        System.out.print("Email du notaire : ");
                        emailNotaire = tt.nextLine();
                        promesseNotaire = serial.getClient(emailNotaire);
                    }
                    if(promesse!=null && promesseAcheteur!=null && promesseVendeur!=null && promesseNotaire!=null) {
                        serial.addPromesseDeVente(valeur, promesse, promesseAcheteur, promesseVendeur, promesseNotaire);
                    }else {
                        if(promesse == null && promesseAcheteur == null && promesseVendeur == null && promesseNotaire == null )
                            System.out.print("Les informations incorrectes  \n");
                    }retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 8:
                System.out.println("Afficher la liste des promesses :");
                System.out.print(serial.afficherPromesseDeVente());
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 9:
                    String mandatAdresse= null, mandatEmail = null,mandatDuree= null;
                    BienImmobilier mandat= null; Client mandatVend= null;
                    System.out.println("Creer un mandat :");
                    Scanner cc = new Scanner(System.in);
                    while(mandatAdresse==null && mandatEmail==null && mandatDuree == null) {
                        System.out.print("Email du vendeur  : ");mandatEmail = cc.nextLine();
                        mandatVend = serial.getClient(mandatEmail);
                        System.out.print("L'adresse   : ");mandatAdresse = cc.nextLine();
                        mandat =serial.getBien(mandatAdresse);
                        System.out.print("Durée du mandat : ");mandatDuree = cc.nextLine();
                    }
                    if(mandatVend!=null && mandat!=null ) {
                        System.out.println("Le mandat et enregister ");
                        serial.addMandat(mandat, mandatVend, mandatDuree);
                    }else {
                        if(mandat == null && mandatVend == null )
                            System.out.print("les données sont incorrectes \n");
                    }retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 10:
                    System.out.print("Afficher la liste des mandats  : ");
                    System.out.print(serial.afficherMandats());
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;
                case 11:
                    String pubtxt= null, pubhtml = null, pubimage = null, pubvideo= null;
                    int numPub =0 ;
                    Scanner lirePublicite = new Scanner(System.in);
                    System.out.println("Type de publicité à ajouter : ");
                    System.out.print(" \n 1.Texte \t  2.Html \t 3.Image \t 4.Video \n" );
                    while (numPub != 1 && numPub != 2 && numPub!=3 && numPub!=4) {
                        try {
                            System.out.print(" choisir le type du pub  : ");
                            numPub = clavier.nextInt();
                        } catch(InputMismatchException e) {
                            clavier.next();
                        }
                        if(numPub ==1) {
                            System.out.print(" ajouter un text : ");
                            pubtxt = lirePublicite.nextLine();
                        }if(numPub ==2) {
                            System.out.print(" ajouter un Ajouter un lien html : ");
                            pubhtml= lirePublicite.nextLine();
                        }
                        if(numPub ==3) {
                            System.out.print(" ajouter un Ajouter une image : ");
                            pubimage= lirePublicite.nextLine();
                        } if(numPub ==4) {
                            System.out.print(" ajouter un Ajouter une video : ");
                            pubvideo= lirePublicite.nextLine();
                        }
                    }retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;
                case 12:
                    System.out.println("Afficher la liste du publiciter  : ");
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 13:
                    int choixVoeux = 0; float prixVoeux = 0;
                    Voeux.TypeDeBien typeVoeux = null;
                    System.out.println("Votre bien ? :");
                    System.out.print("1. Appartement \t\t\t 2. Maison \t\t\t 3. Terrain \n");
                    while (choixVoeux != 1 && choixVoeux != 2 && choixVoeux != 3) {
                        try {
                            System.out.print("quell est votre choix : ");
                            choixVoeux = clavier.nextInt();
                        } catch(InputMismatchException e) {
                            clavier.next();
                        }
                    }
                    if(choixVoeux==1) typeVoeux = Voeux.TypeDeBien.Appartement;
                    if(choixVoeux==2) typeVoeux = Voeux.TypeDeBien.Maison;
                    if(choixVoeux==3) typeVoeux = Voeux.TypeDeBien.Terrain;
                    while (prixVoeux<=0 ) { System.out.print(" Votre prix du voeux : ");
                    prixVoeux = clavier.nextFloat();
                    }
                    System.out.println(serial.addVoeux(typeVoeux,prixVoeux));
                    retour = menu();
                    if(retour==1) { choix = -1; }
                    else{ choix = 0; }
                    break;

                case 14:
                    System.out.print("Afficher les statistiques :\n\n");
                    System.out.print(serial.afficherStatistique());
                    retour = menu();
                    if(retour==1) {
                        choix = -1;
                    }else{
                        choix = 0;
                    }
            }
        }
    }
}