package Timmo.Agence;

import Timmo.BienImmobilier.BienImmobilier;
import Timmo.Client.Client;

import java.io.Serializable;

public class RDV implements Serializable{
    private static final long serialVersionUID= 1014L;


    public enum RdvPour {Visite, Vente, Mandat};
    private String dateRdv;
    private Client client;
    private RdvPour pour;
    private BienImmobilier bien;

    public RDV(String date, Client client, RdvPour pour, BienImmobilier bien) {
        this.dateRdv = date;
        this.client = client;
        this.pour = pour;
        this.bien = bien;
    }

    public String getDateRdv() {
        return dateRdv;
    }

    public void setDateRdv(String dateRdv) {
        this.dateRdv = dateRdv;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public RdvPour getPour() {
        return pour;
    }

    public void setPour(RdvPour pour) {
        this.pour = pour;
    }

    public BienImmobilier getBien() {
        return bien;
    }

    public void setBien(BienImmobilier bien) {
        this.bien = bien;
    }

    public String toString() {
        return " RDV " +this.getBien().toString()+ ",date : " +this.getDateRdv()+", Le RDV pour : "+this.getPour()+"\n"+
                this.getClient()+"\n";
    }
}

