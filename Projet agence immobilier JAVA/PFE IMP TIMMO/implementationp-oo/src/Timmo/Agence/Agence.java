package Timmo.Agence;
import Timmo.Client.Client;
import Timmo.BienImmobilier.Appartement;
import Timmo.BienImmobilier.BienImmobilier;
import Timmo.BienImmobilier.Maison;
import Timmo.BienImmobilier.Terrain;
import Timmo.Client.Voeux;
import Timmo.Publicité.Publicite;
import java.io.*;
import java.util.ArrayList;
import java.io.Serializable;

    public class Agence implements Serializable {
        private static final long serialVersionUID= 1010L;

        private ArrayList<BienImmobilier> bi;
        private ArrayList<RDV> rd;
        private ArrayList<Client> clt;
        private ArrayList<PromesseDeVente> prm;
        private ArrayList<MandatDeVente> md;
        public enum choixClient{Physique, Morale};
        public enum leBien{Appartement,Maison, Terrain};
        static public ArrayList<Publicite> pub;

        public Agence() {
            this.pub = new ArrayList<>();
            this.bi = new ArrayList<>();
            this.rd = new ArrayList<>();
            this.clt = new ArrayList<>();
            this.prm = new ArrayList<>();
            this.md = new ArrayList<>();
        }

        public void addClient(Client c, Voeux v) {
            c.setVoeux(v);
            this.clt.add(c);
        }
        public ArrayList<Client> getCls() {
            return this.clt;
        }
        public String afficherClt() {
            String cpt = "\t";
            for(Client p : this.getCls()) {
                cpt += p.toString()+"\n";
            }return cpt;
        }
        public void addBien(BienImmobilier b) {
            this.bi.add(b);
        }
        public String afficherBiens() {
            String cpt = "\t";
            for(BienImmobilier ba : this.getle()) {
                cpt += ba.toString()+"\n";
            }return cpt; }

        public ArrayList<RDV> getRdv() {
            return rd;
        }
        public void addRdv(RDV r) {
            this.rd.add(r);
        }
        public String afficherRDV() {
            String cpt = "\t";
            for(RDV rr : this.getRdv()) {
                cpt += rr.toString()+"\n";
            }return cpt;
        }
        public void siPromesse(PromesseDeVente promesse) {
            promesse.setSigne();
        }
        public ArrayList<PromesseDeVente> getPromesses() {
            return prm;
        }
        public void addPromesseDeVente(float val, BienImmobilier bien, Client achet, Client vend, Client not) {
            if (this.getle().contains(bien)) {
                float prix = bien.getPrix();
                Commission commission = new Commission(prix, val);
                PromesseDeVente addpromesse = new PromesseDeVente(achet, vend, not, bien, commission);
                this.siPromesse(addpromesse);
                this.prm.add(addpromesse);
                bien.setVendre();
            }
        }
        public String afficherPromesseDeVente() {
            String cpt = "\t";
            for (PromesseDeVente pr : this.getPromesses()) {
                cpt += pr.toString() + "\n";
            }return cpt;
        }
        public void addMandat(BienImmobilier bien,Client vendeur,String duree) {
            if(this.bi.contains(bien) && this.getVds().contains(vendeur)) {
                MandatDeVente mdt = new MandatDeVente(duree,vendeur);
                mdt.signee();
                this.md.add(mdt);
                bien.setMandat(mdt);
            }
        }
        public String afficherMandats() {
            String cpt = "\t";
            for(MandatDeVente mv : this.md) {
                cpt += mv.toString()+"\n";
            }return cpt;
        }
        public ArrayList<BienImmobilier> addVoeux(Voeux.TypeDeBien typeVoeux, float voeuxPrix) {
            ArrayList<BienImmobilier> listBien = new ArrayList<BienImmobilier>();
            if (typeVoeux == Voeux.TypeDeBien.Maison) {
                for (BienImmobilier bv : this.bi) {
                    if (bv instanceof Maison && bv.getPrix() <= voeuxPrix)
                        listBien.add(bv);
                }
            }if (typeVoeux == Voeux.TypeDeBien.Appartement) {
                for (BienImmobilier bv : this.bi) {
                    if (bv instanceof Appartement && bv.getPrix() <= voeuxPrix)
                        listBien.add(bv);
                }
            }if (typeVoeux == Voeux.TypeDeBien.Terrain) {
                for (BienImmobilier bv : this.bi) {
                    if (bv instanceof Terrain && bv.getPrix() <= voeuxPrix)
                        listBien.add(bv);
                }
            }return listBien;
        }

        public BienImmobilier getBien(String adresseBi) {
            BienImmobilier bien = null;
            for (BienImmobilier ba : this.bi) {
                if (ba.getAdresse().equals(adresseBi)) {
                    bien = ba;
                }
            }return bien;
        }
        public Client getClient(String emailbi) {
            Client client = null;
            for (Client cl : this.getCls()) {
                if (cl.getEmail().equals(emailbi)) {
                    client = cl;
                }
            }return client;
        }

        public ArrayList<BienImmobilier> getle() {
            ArrayList<BienImmobilier> gtBien = new ArrayList<BienImmobilier>();
            for(BienImmobilier bi : this.bi) {
                if(!bi.okVendre())
                    gtBien.add(bi);
            }return gtBien;
        }

        public ArrayList<BienImmobilier> getBienVend() {
            ArrayList<BienImmobilier> sb = new ArrayList<BienImmobilier>();
            for (BienImmobilier bv : this.bi) {
                if (bv.okVendre())
                    sb.add(bv);
            }return sb;
        }

        public ArrayList<Client> getAchet(){
            ArrayList<Client> achet= new ArrayList<Client>();
            for(Client client :this.getCls()) {
                if(client.getStatut()==Client.Statut.Acheteur)
                    achet.add(client);
            }
            return achet;
        }
        public ArrayList<Client> getVds(){
            ArrayList<Client> vend= new ArrayList<Client>();
            for(Client client :this.getCls()) {
                if(client.getStatut()==Client.Statut.Vendeur)
                    vend.add(client);
            }
            return vend;
        }
        public String afficherStatistique(){
            return  " Nombre des vendeurs : "+this.getVds().size()+"\n"+
                    " Nombre des acheteurs : "+this.getAchet().size()+"\n"+
                    " Nombre des biens : "+this.bi.size()+"\n"+
                    " Nombre des Biens vendus : "+this.getBienVend().size()+"\n"+
                    " Pourcentage ventes : "+(100*this.getBienVend().size()/this.bi.size())+" % \n";
        }
        public void serialisation() {
            try ( ObjectOutputStream sv = new ObjectOutputStream(new FileOutputStream("donnéeAgence.csv")) ) {
                sv.writeObject(this);
            } catch (IOException e) {
                System.err.println("Probléme de la sérialisation : " + e);
                System.exit(1);
            }
        }
        public static Agence deserialisation() {
            Agence serial = new Agence();
            try ( ObjectInputStream stk = new ObjectInputStream(new FileInputStream("donnéeAgence.csv")) ) {
                serial = (Agence)stk.readObject();
            } catch (IOException e) {
                System.err.println("Probléme de la désérialisation : " + "e");
               System.exit(2);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return serial;
        }
    }



