package Timmo.Agence;

import java.io.Serializable;

public class Commission implements Serializable {
    private static final long serialVersionUID= 1011L;


    private float prixCommission;
    private float valCommision;

        public Commission(float prix, float val) {
            this.prixCommission = prix;
            this.valCommision = val;
        }

    public float getPrixCommission() {
        return prixCommission;
    }

    public void setPrixCommission(float prixCommission) {
        this.prixCommission = prixCommission;
    }

    public float getValCommision() {
        return valCommision;
    }

    public void setValCommision(float valCommision) {
        this.valCommision = valCommision;
    }
    public float getCalculCommission() {
        return (float) this.prixCommission*this.valCommision/100;
    }

    public String toString() {
            return " La commission est : "+this.getCalculCommission()+", le prix : "+this.getPrixCommission()+", la valeur du commision : "+this.getValCommision();
        }


    }

