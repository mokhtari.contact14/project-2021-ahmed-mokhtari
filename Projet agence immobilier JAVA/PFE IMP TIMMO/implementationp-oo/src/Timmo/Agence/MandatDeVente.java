package Timmo.Agence;
import Timmo.Client.Client;
import java.io.Serializable;

    public class MandatDeVente implements Serializable {
        private static final long serialVersionUID= 1012L;

        private String duree;
        private  Client vendeur;
        private boolean signee;

        public MandatDeVente(String duree, Client vendeur) {
            this.duree = duree;
            this.vendeur = vendeur;
            this.signee = false;
        }

        public String getDuree() {
            return duree;
        }

        public void setDuree(String duree) {
            this.duree = duree;
        }

        public Client getVendeur() {
            return this.vendeur;
        }

        public void setVendeur(Client client) {
            this.vendeur = client;
        }

        public boolean isSignee() {
            return signee;
        }

        public void signee() {
            this.signee = true;
        }

        public String toString() {
            return " Manda : vendeur est "+this.getVendeur()+", la durée du mandat est : "+this.getDuree()+", le mandat est : "+(this.isSignee()?" signée ": " il est pas  signée");
        }


    }


