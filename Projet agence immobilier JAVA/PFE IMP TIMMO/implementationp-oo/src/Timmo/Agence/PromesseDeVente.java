package Timmo.Agence;

import Timmo.BienImmobilier.BienImmobilier;
import Timmo.Client.Client;

import java.io.Serializable;

public class PromesseDeVente implements Serializable {
    private static final long serialVersionUID= 1013L;


    private Client acheteur;
    private Client vendeur;
    private Client notaire;
    private BienImmobilier bien;
    private Commission commission;
    private boolean signe;

    public PromesseDeVente(Client acheteur, Client vendeur, Client notaire, BienImmobilier bien, Commission commission) {
        this.acheteur = acheteur;
        this.vendeur = vendeur;
        this.notaire = notaire;
        this.bien = bien;
        this.commission = commission;
        this.signe = false;
    }

    public Client getAcheteur() {
        return acheteur;
    }

    public void setAcheteur(Client acheteur) {
        this.acheteur = acheteur;
    }

    public Client getVendeur() {
        return vendeur;
    }

    public void setVendeur(Client vendeur) {
        this.vendeur = vendeur;
    }

    public Client getNotaire() {
        return notaire;
    }

    public void setNotaire(Client notaire) {
        this.notaire = notaire;
    }

    public BienImmobilier getBien() {
        return bien;
    }

    public void setBien(BienImmobilier bien) {
        this.bien = bien;
    }

    public Commission getCommission() {
        return commission;
    }

    public void setCommission(Commission commission) {
        this.commission = commission;
    }

    public boolean isSignee() {
        return signe;
    }

    public void setSigne(boolean signee) {
        this.signe = true;
    }
    public void setSigne() {
        this.signe = true;
    }

    @Override
    public String toString() {
        return " PromesseDeVente : " +
                ",  acheteur est : " + this.getAcheteur() +
                ",  vendeur est  : " + this.getVendeur() +
                ",  otaire est  : " + this.getNotaire() +
                ",  la promesse est : " +(this.isSignee()?" signée":"il est pas  signée");
    }
}

