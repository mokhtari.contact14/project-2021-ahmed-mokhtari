package Timmo.Publicité;

import Timmo.BienImmobilier.BienImmobilier;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Publicite implements Serializable {
    private static final long serialVersionUID= 1040L;

    Doc type;
    ArrayList<BienImmobilier> biens = new ArrayList<BienImmobilier>();

    public Publicite(Doc type, ArrayList<BienImmobilier> biens) {
        this.type = type;
        this.biens = biens;
    }

    @Override
    public String toString() {
        return "Publicite{" +
                "type=" + type +
                ", biens=" + biens +
                '}';
    }
}


