package Timmo.Publicité;

import Timmo.Agence.Agence;
import Timmo.BienImmobilier.BienImmobilier;

import java.io.Serializable;
import java.util.ArrayList;

public class JournauxLocaux extends Publicite implements Serializable {
    private static final long serialVersionUID= 1042L;

    public JournauxLocaux(Doc type, ArrayList<BienImmobilier> biens) {
        super(type, biens);
    }

    @Override
    public String toString() {
        return "JournauxLocaux{" +
                "type=" + type +
                ", biens=" + biens +
                '}';
    }
}
