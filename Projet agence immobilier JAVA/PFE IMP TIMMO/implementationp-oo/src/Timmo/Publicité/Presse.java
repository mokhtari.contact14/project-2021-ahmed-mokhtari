package Timmo.Publicité;

import Timmo.Agence.Agence;
import Timmo.BienImmobilier.BienImmobilier;

import java.io.Serializable;
import java.util.ArrayList;

public class Presse extends Publicite implements Serializable {
    private static final long serialVersionUID= 1043L;

    public Presse(Doc type, ArrayList<BienImmobilier> biens) {
        super(type, biens);
    }

    @Override
    public String toString() {
        return "Presse{" +
                "type=" + type +
                ", biens=" + biens +
                '}';
    }
}
