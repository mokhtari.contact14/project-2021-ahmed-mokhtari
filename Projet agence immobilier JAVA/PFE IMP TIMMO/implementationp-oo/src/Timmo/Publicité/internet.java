package Timmo.Publicité;

import Timmo.Agence.Agence;
import Timmo.BienImmobilier.BienImmobilier;

import java.io.Serializable;
import java.util.ArrayList;

public class internet extends Publicite implements Serializable {
    private static final long serialVersionUID= 1041L;

    public internet(Doc type, ArrayList<BienImmobilier> biens) {
        super(type, biens);
    }

    @Override
    public String toString() {
        return "internet{" +
                "type=" + type +
                ", biens=" + biens +
                '}';
    }
}
