package Timmo.Client;

import java.io.Serializable;

public class Voeux implements Serializable {
    private static final long serialVersionUID= 1033L;


    public enum TypeDeBien{Maison,Appartement, Terrain}
        private TypeDeBien typeDeBien;
        private float prixVoeux;
        private String Adresse;

        public Voeux(TypeDeBien typeDeBien, float prix, String adresse) {
            this.typeDeBien = typeDeBien;
            this.prixVoeux = prix;
            this.Adresse = adresse;
        }

    public Voeux.TypeDeBien getTypeDeBien() {
        return typeDeBien;
    }

    public void setTypeDeBien(Voeux.TypeDeBien typeDeBien) {
        typeDeBien = typeDeBien;
    }

    public float getprixVoeux() {
        return prixVoeux;
    }

    public void setprixVoeux(float prixSouhaite) {
        this.prixVoeux = prixSouhaite;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    @Override
    public String toString() {
        return " le prix du Voeux: "+this.getprixVoeux()+"  l'adresse est : "+this.getAdresse()+".";
    }
}


