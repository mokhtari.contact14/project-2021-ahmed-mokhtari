package Timmo.Client;

import java.io.Serializable;
    public abstract class Client implements Serializable {
        private static final long serialVersionUID= 1030L;


        private String adresse;
        private String numTel;
        private String email;
        private Statut statut;
        private Voeux voeux;
        public enum Statut{Acheteur, Vendeur, Notaire};

        public Client(String adresse, String numero, String email, Statut statut) {
            this.adresse = adresse;
            this.numTel = numero;
            this.email = email;
            this.statut = statut;
            this.voeux = null;
        }
        public String getAdresse(){
            return adresse;
        }

        public void setAdresse(String adresse) {
            this.adresse = adresse;
        }

        public String getNumTel() {
            return numTel;
        }

        public void setNumTel(String numero) {
            this.numTel = numero;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Statut getStatut() {
            return statut;
        }

        public void setStatut(Statut statut) {
            this.statut = statut;
        }

        public Voeux getVoeux() {
            return voeux;
        }

        public void setVoeux(Voeux voeux) {
            this.voeux = voeux;
        }

        @Override
        public String toString() {
            return  "l'adresse  : " + adresse + '\'' +
                    ", numero du télephone : " + numTel + '\'' +
                    ", email du client : " + email + '\'' +
                    ", statut : " + statut +
                    ", voeux : " + voeux +
                    '}';
        }
    }
