package Timmo.Client;
import java.io.Serializable;

public class PersonnePhysique extends Client implements Serializable {
    private static final long serialVersionUID = 1032L;


    private String nom;
    private String prenom;

    public PersonnePhysique(String adresse, String numero, String email, Statut statut, String nom, String prenom) {
        super(adresse, numero, email, statut);
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "PersonnePhysique :  " +
                " nom  : " + nom + '\'' +
                " prenom  : " + prenom + '\'' +
                 super.toString();
    }

}


