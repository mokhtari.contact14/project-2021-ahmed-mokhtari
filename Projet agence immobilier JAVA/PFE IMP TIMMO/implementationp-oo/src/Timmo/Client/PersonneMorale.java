package Timmo.Client;
import java.io.Serializable;

public class PersonneMorale extends Client implements Serializable {
    private static final long serialVersionUID = 1031L;
    private String typeMorale;
    private int numSiren;
    private String nom;

    public PersonneMorale(String adresse, String numero, String email, Statut statut, String nom, String typeJuridique, int siren) {
        super(adresse, numero, email, statut);
        this.typeMorale = typeJuridique;
        this.numSiren = siren;
        this.nom = nom;
    }

    public String getTypeMorale() {
        return typeMorale;
    }

    public void setTypeMorale(String typeMorale) {
        this.typeMorale = typeMorale;
    }

    public int getNumSiren() {
        return numSiren;
    }

    public void setNumSiren(int numSiren) {
        this.numSiren = numSiren;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public String toString() {
        return " Personne morale :  Nom : "+this.getNom()+", le type juridique  : "+this.getTypeMorale()+", Numero du siren: "+this.getNumSiren()+", "+super.toString();
    }
}




