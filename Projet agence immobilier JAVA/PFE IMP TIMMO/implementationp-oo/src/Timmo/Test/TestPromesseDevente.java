package Timmo.Test;

import Timmo.Agence.Commission;
import Timmo.Agence.PromesseDeVente;
import Timmo.BienImmobilier.Appartement;
import Timmo.BienImmobilier.BienImmobilier;
import Timmo.Client.Client;
import Timmo.Client.PersonnePhysique;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestPromesseDevente {
    private PromesseDeVente pr;
    Client acht = new PersonnePhysique("tls", "0000", "@gmail", Client.Statut.Acheteur, "ahmed", "xxxx");
    Client vend = new PersonnePhysique("rak", "00000", "@gmail", Client.Statut.Vendeur, "viid", "sparta");
    Client notaire = new PersonnePhysique("rak", "075", "@gmail", Client.Statut.Vendeur, "not", "no");
    BienImmobilier lebien = new Appartement(400000 ,"toulouse", "17/072021", BienImmobilier.Orientation.Nord,4,3,150);
    Commission commission = new Commission(150000, 120);
    private PromesseDeVente promesse;
    @Before
    public void test() throws Exception {
        this.promesse = new PromesseDeVente(acht, vend, notaire, lebien ,commission);
    }
    @After
    public void testEx() throws Exception {
        this.promesse = null;
    }
    @Test
    public void testAcheteur(){
        this.promesse.setVendeur(acht);
        assertTrue(this.promesse.getAcheteur().equals(acht));
    }
    @Test
    public void testVendeur(){
        this.promesse.setVendeur(vend);
        assertTrue(this.promesse.getVendeur().equals(vend));
    }
    @Test
    public void testComission(){
        this.promesse.setCommission(commission);
        assertTrue(this.promesse.getCommission().equals(commission));
    }
    @Test
    public void testNotaire(){
        this.promesse.setNotaire(notaire);
        assertTrue(this.promesse.getNotaire().equals(notaire));
    }

    @Test
    public void testBien(){
        this.promesse.setBien(lebien);
        assertTrue(this.promesse.getBien().equals(lebien));
    }

}

