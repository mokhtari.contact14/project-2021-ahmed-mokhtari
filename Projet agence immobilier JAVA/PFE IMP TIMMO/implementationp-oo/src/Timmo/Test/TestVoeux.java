package Timmo.Test;

import Timmo.Client.Voeux;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestVoeux {
    private Voeux testvoeux;
    @Before
    public void test() throws Exception {
        testvoeux = new Voeux(Voeux.TypeDeBien.Appartement, 500000F, "toulouse");
    }
    @After
    public void testEx() throws Exception {
        testvoeux = null;
    }
    @Test
    public void testClasseVoeux() {
        this.testvoeux.setTypeDeBien(Voeux.TypeDeBien.Appartement); this.testvoeux.setAdresse("toulouse"); this.testvoeux.setprixVoeux(100000);
        assertTrue(this.testvoeux.getAdresse() == "toulouse" && this.testvoeux.getprixVoeux() == 100000 && this.testvoeux.getTypeDeBien() == Voeux.TypeDeBien.Appartement);
    }

}

