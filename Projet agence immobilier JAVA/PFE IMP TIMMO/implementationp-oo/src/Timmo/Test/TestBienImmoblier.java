package Timmo.Test;

import Timmo.BienImmobilier.Appartement;
import Timmo.BienImmobilier.BienImmobilier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestBienImmoblier {
    private Appartement apparttement;
    @Before
    public void test() throws Exception {
        this.apparttement = new Appartement(25000,"toulouse", "14/07/2021", BienImmobilier.Orientation.Sud,5, 1, 100);
    }
    @After
    public void testEx() throws Exception {
        this.apparttement = null;
    }

    @Test
    public void testNombreEtage(){
        this.apparttement.setNumeroEtage(1);
        assertTrue(this.apparttement.getNumeroEtage() == 1 );
    }

    @Test
    public void testNombrePiece(){
        this.apparttement.setNbreDePiece(5);
        assertTrue(this.apparttement.getNbreDePiece() == 5 );
    }

    @Test
    public void testGetAndSetChargeMens(){
        this.apparttement.setChargeMensuelle(100);
        assertTrue(this.apparttement.getChargeMensuelle() == 100);
    }

}
