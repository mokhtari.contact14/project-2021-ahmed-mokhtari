package Timmo.Test;

import Timmo.Agence.MandatDeVente;
import Timmo.Client.Client;
import Timmo.Client.PersonnePhysique;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestMandat {
    private MandatDeVente mandat;
    Client acht = new PersonnePhysique("tls", "06060660", "@gmail", Client.Statut.Acheteur, "ahmed", "xxxx");
    Client vend = new PersonnePhysique("rak", "00000", "@gmail", Client.Statut.Vendeur, "viid", "sparta");
    @Before
    public void test() throws Exception {
        mandat = new MandatDeVente("15/03/2021", acht);
    }
    @After
    public void testEx() throws Exception {
        mandat = null;
    }
    @Test
    public void testDuree(){
        this.mandat.setDuree("15/03/2021");
        assertTrue(this.mandat.getDuree() == "15/03/2021");
    }
    @Test
    public void testAchteur(){
        this.mandat.setVendeur(acht);
        assertTrue(this.mandat.getVendeur().equals(acht));
    }
}
