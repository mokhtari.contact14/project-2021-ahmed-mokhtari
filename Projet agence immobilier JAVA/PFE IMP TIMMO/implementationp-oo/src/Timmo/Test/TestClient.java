package Timmo.Test;
import Timmo.Client.Voeux;
import Timmo.Client.Client;
import Timmo.Client.PersonnePhysique;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestClient {
    private Client clienPhysique;
    @Before
    public void test() throws Exception {
        this.clienPhysique = new PersonnePhysique(" adresse physique", "0656780638", "ahmed@mokhtari.com", Client.Statut.Vendeur, "MOKHTARI", "AHMED");
    }
    @After
    public void testEx() throws Exception {
        this.clienPhysique = null;
    }
    @Test
    public void testClassClient() {
        Voeux voeux = new Voeux(Voeux.TypeDeBien.Maison, 1500000, "toulouse");
        this.clienPhysique.setAdresse("newAdressePhysic");
        this.clienPhysique.setEmail("newEmailPhysic");
        this.clienPhysique.setNumTel("0664286973");

        this.clienPhysique.setStatut(Client.Statut.Notaire);
        this.clienPhysique.setVoeux(voeux);
        assertTrue(this.clienPhysique.getAdresse() == "newAdressePhysic"
                && this.clienPhysique.getEmail() == "newEmailPhysic"
                && this.clienPhysique.getNumTel() == "0664286973"
                && this.clienPhysique.getStatut() == Client.Statut.Notaire
                && this.clienPhysique.getVoeux().getAdresse() == "toulouse");
    }
}



