package Timmo.Test;

import Timmo.Agence.RDV;
import Timmo.BienImmobilier.Appartement;
import Timmo.BienImmobilier.BienImmobilier;
import Timmo.Client.Client;
import Timmo.Client.PersonnePhysique;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestRdv {
    private RDV rdv;
    Client ahmed = new PersonnePhysique("tls", "0000", "@gmail", Client.Statut.Acheteur, "ahmed", "xxxx");
    BienImmobilier appart = new Appartement(2500 ,"toulouse", "14/03/2021", BienImmobilier.Orientation.Nord,4, 3, 2);

    @Before
    public void test() throws Exception {
        this.rdv = new RDV("15/02/2020", ahmed, RDV.RdvPour.Mandat, appart);
    }
    @After
    public void testEx() throws Exception {
        this.rdv = null;
    }
    @Test
    public void testDateRdv(){
        this.rdv.setDateRdv("14/03/2021");
        assertTrue(this.rdv.getDateRdv() == "14/03/2021");
    }

}
