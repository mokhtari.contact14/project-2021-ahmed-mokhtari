import math
import pygame
import sys
import tkinter
import numpy

class Game_Puissance4:
    def __init__(self):
        self.ncl = 7
        self.nlg = 6
        self.Joueur = 1
        self.IA = 2
        self.msg = ""
        self.tabl = numpy.zeros((self.nlg, self.ncl))

    def position(self,gril,lg, cl, posi):
        gril[lg][cl] = posi

    def ok_position(self, gril,cl):
        return gril[5][cl] == 0

    def get_position(self,gril):
        ls = []
        for cl in range(self.ncl):
            if self.ok_position(gril,cl):
                ls.append(cl)
        return ls

    def next_lg(self, gril,cl):
        for lg in range(self.nlg):
            if gril[lg][cl] == 0:
                return lg

    def gagne(self, gril,posi):
        for cl in range(self.ncl):
            for lg in range(3):
                if gril[lg][cl] == posi and gril[lg+1][cl] == posi and gril[lg+2][cl] == posi and gril[lg+3][cl] == posi:
                    return True
        for lg in range(self.nlg):
            for cl in range(4):
                if gril[lg][cl] == posi and gril[lg][cl+1] == posi and gril[lg][cl+2] == posi and gril[lg][cl+3] == posi:
                    return True
        for cl in range(4):
            for lg in range(3, self.nlg):
                if gril[lg][cl] == posi and gril[lg-1][cl+1] == posi and gril[lg-2][cl+2] == posi and gril[lg-3][cl+3] == posi:
                    return True
        for cl in range(4):
            for lg in range(3):
                if gril[lg][cl] == posi and gril[lg+1][cl+1] == posi and gril[lg+2][cl+2] == posi and gril[lg+3][cl+3] == posi:
                    return True

    def final_ch(self,gril):
        return  len(self.get_position(gril)) == 0 or self.gagne(gril,self.Joueur) or self.gagne(gril,self.IA)
    def val(self,lst,posi):
        val = 0
        id = self.Joueur
        if lst.count(id) == 3 and lst.count(0) == 1:
            val -= 10
        elif lst.count(id) == 4 :
            val -= 12
        elif lst.count(id) == 2 and lst.count(0) == 2:
            val -= 8
        if lst.count(posi) == 4:
            val += 80
        elif lst.count(posi) == 3 and lst.count(0) == 1:
            val +=40
        elif lst.count(posi) == 2 and lst.count(0) == 2:
            val += 10
        return val

    def id_val(self, gril,posi):
        val = 0
        for lg in range(self.nlg):
            t = [i for i in list(gril[lg,:])]
            for cl in range(4):
                lst = t[cl:cl+4]
                val += self.val(lst, posi)
        for cl in range(self.ncl):
            t = [i for i in list(gril[:,cl])]
            for lg in range(3):
                lst = t[lg:lg+4]
                val += self.val(lst, posi)
        for lg in range(3):
            for cl in range(4):
                lst = [gril[lg+i][cl+i] for i in range(4)]
                val += self.val(lst, posi)
        for lg in range(3):
            for cl in range(4):
                lst = [gril[lg+3-i][cl+i] for i in range(4)]
                val += self.val(lst, posi)
        return val

    def min_max(self,gril, fx, alpha, beta,minmax_joueur):
        ok_position = self.get_position(gril)
        if self.final_ch(gril):
            if self.gagne(gril,self.IA):
                return None, math.inf
            elif self.gagne(gril,self.Joueur):
                return None, -math.inf
            else:
                return None, 0
        if fx == 0:
            return None, self.id_val(gril,self.IA)
        if minmax_joueur:                            # CAS (IA)
            value = -math.inf
            rs = None
            for cl in ok_position:
                lg = self.next_lg(gril,cl)
                lst = gril.copy()
                self.position(lst, lg, cl, self.IA)
                if self.min_max(lst, fx-1, alpha, beta,False)[1] > value:
                    value = self.min_max(lst, fx-1, alpha, beta,False)[1]
                    rs = cl
                alpha = max(alpha, value)
                if alpha >= beta:
                    break
            return rs, value
        else:                      # else FAUX == c'est (L'UTILISATEUR)
            value = math.inf
            rs = None
            for cl in ok_position:
                lg = self.next_lg(gril,cl)
                lst = gril.copy()
                self.position(lst, lg, cl,self.Joueur)
                if self.min_max(lst, fx-1, alpha, beta,True)[1] < value:
                    value = self.min_max(lst, fx-1, alpha, beta,True)[1]
                    rs = cl
                beta = min(beta, value)
                if alpha >= beta:
                    break
            return rs, value
    def __str__(self):
        return str(self.tabl)

    def Int_graphique(self,aff):
        for cl in range(self.ncl):
            for lg in range(self.nlg):
                pygame.draw.rect(aff, (0,0,0), (cl*100, lg*100+100, 100, 100))
                pygame.draw.circle(aff, (255,255,255), (int(cl*100+100/2), int(lg*100+100+100/2)), int(100/2 - 5))

        for cl in range(self.ncl):
            for lg in range(self.nlg):
                if self.tabl[lg][cl] == self.Joueur:
                    pygame.draw.circle(aff, (255,0,0), (int(cl*100+100/2), (7*100)-int(lg*100+100/2)), int(100/2 - 5))
                elif self.tabl[lg][cl] == self.IA:
                    pygame.draw.circle(aff, (0,255,0), (int(cl*100+100/2), (7*100)-int(lg*100+100/2)), int(100/2 - 5))
        pygame.display.update()

    def Go_gaming(self):
        pygame.init()
        aff = pygame.display.set_mode((7*100, 7*100))
        self.Int_graphique(aff)
        pygame.display.update()
        install = tkinter.Tk()
        boxe_prise = tkinter.IntVar()
        tkinter.Label(install, text ="Qui va lancer le jeux ?",font=("antialias", 35)).grid(row = 0, column = 0)
        tkinter.Radiobutton(install, text="IA", font=("antialias", 30), variable=boxe_prise, value="0").grid(row=1, column=0)
        tkinter.Radiobutton(install, text="Joueur", font=("antialias", 30), variable=boxe_prise, value="1").grid(row=2, column=0)
        tkinter.Button(install, text ="Confirmer",font=("antialias", 30), command=install.destroy).grid(row = 3, column = 0)
        install.mainloop()
        cas = boxe_prise.get()
        game_ff = False
        while not game_ff:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.MOUSEMOTION:
                    pygame.draw.rect(aff, (224,224,224), (0,0, 7*100, 100))
                    rt = event.pos[0]
                    if cas == 1:
                        pygame.draw.circle(aff, (255,0,0), (rt, int(100/2)), int(100/2 - 5))
                pygame.display.update()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    pygame.draw.rect(aff, (0,0,0), (0,0, 7*100, 100))
                    if cas == 1:
                        rt = event.pos[0]
                        cl = int(math.floor(rt/100))
                        if self.ok_position(self.tabl,cl):
                            lg = self.next_lg(self.tabl,cl)
                            self.position(self.tabl,lg,cl,self.Joueur)

                            if self.gagne(self.tabl,self.Joueur):
                                self.msg = "L'utilisateur est gagne"
                                game_ff = True
                            cas = 0
                            print(self)
                            self.Int_graphique(aff)
            if cas == 0 and not game_ff:
                cl = self.min_max(self.tabl,4,-math.inf, math.inf,True)[0]

                if self.ok_position(self.tabl,cl):
                    lg = self.next_lg(self.tabl,cl)
                    self.position(self.tabl,lg,cl,self.IA)
                    if self.gagne(self.tabl,self.IA):
                        self.msg = "OPPS !! IA GAGNE "
                        game_ff = True
                    print(self)
                    self.Int_graphique(aff)
                    cas =1
            if game_ff:
                if(self.msg!= "OPPS !! IA GAGNE" and self.msg!="L'utilisateur est gagne"):
                    self.msg = "OPPS !! IA GAGNE "
                install = tkinter.Tk()
                boxe_prise = tkinter.IntVar()
                tkinter.Label(install, text =self.msg,font=("antialias", 35)).grid(row = 0, column = 0)
                tkinter.Radiobutton(install, text="Rejouer",font=("antialias", 20), variable=boxe_prise, value="1").grid(row = 1, column = 0)
                tkinter.Radiobutton(install, text="Exit", font=("antialias", 20), variable=boxe_prise, value="0").grid(row=2, column=0)
                tkinter.Button(install, text ="Confirmer", command=install.destroy).grid(row = 3, column = 0)
                install.mainloop()
                Game=Game_Puissance4()
                print(Game)
                Game.play()

if __name__=="__main__":
    Game=Game_Puissance4()
    print(Game)
    Game.Go_gaming()